<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'miceconp_database');

/** MySQL database username */
define('DB_USER', 'miceconp_admin');

/** MySQL database password */
define('DB_PASSWORD', '7ZUeWbRx6uU5');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nUasvJ,S6XoX!3yFKgIM6-7|1nakbXRt92GF:,S+>8&?^b9U9pAL_mi1O-]tT0GU');
define('SECURE_AUTH_KEY',  '7_e3&`%^{ :/=+7U.V} A$jvQ0A_+NiXfihoU%{Z$K)koTSt`Sq5J1Vf?vbF>4j%');
define('LOGGED_IN_KEY',    'T%@Kh/@d+Ulet)~i}fv>9/,j`V*r::KEaj|P4D<U^ve!k:TIC[xksiF^SMDwzR>,');
define('NONCE_KEY',        '2B|opM0gKbV%18Ork0Pp5*`.[*:7`Z2KcEB+Ey<])``R<z|o|.o_QMA:7})v|#+}');
define('AUTH_SALT',        'nL#N|+Cb::T4|RdEl__%6h1C/T_,:N&$ G_8DvkOF+c~E1JV/g@zhyBg3;:0{jIg');
define('SECURE_AUTH_SALT', '{G-s.94ejQ%;1^q`^U6u`+rS@zrs2%]*glXo@^, }Z;mA+MSZ~*$-+1nC80uk<3Y');
define('LOGGED_IN_SALT',   ';x6AkrA[-[Q-F1!>;3Mz?Q1eL>i+%:5CqlOY!@k.aWYcb,$lx^-+YjnJw%[M3H(N');
define('NONCE_SALT',       'oLD`vU{cs|:ND`B$UOO^ HJCB0az*EQW6=EsDjANir#tA3g!6_ ;GlQ.s_c+x$4+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

define( 'WP_AUTO_UPDATE_CORE', false );
define( 'DISALLOW_FILE_MODS', true );
define( 'DISALLOW_FILE_EDIT', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

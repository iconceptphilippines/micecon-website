<?php get_header(); ?>

	<!-- CONTENT -->
	<div id="page-content" class="single-page">
		<div class="container">
			<div class="content">
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>
						<?php
							$post_object = get_field('relation');
						?>

						<pre>
							<?php var_dump($post_object); ?>
						</pre>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<!-- END CONTENT -->

<?php get_footer(); ?>
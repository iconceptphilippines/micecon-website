<?php  ?>
<?php get_header(); ?>

	<!-- CONTENT -->
	<div id="page-content" class="single-page">
		<div class="container">
			<div class="content">
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
						<h2><?php the_title(); ?></h2>

					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<!-- END CONTENT -->

<?php get_footer(); ?>
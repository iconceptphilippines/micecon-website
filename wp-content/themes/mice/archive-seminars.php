<?php

if(is_user_logged_in()):

	$current_user = wp_get_current_user();
	$user_info_arg = array(
		'post_type'			=> 'users',
		'posts_per_page'	=> 1,
		'author'			=> $current_user->ID
	);
	$user_info = get_posts( $user_info_arg );
endif;

get_header(); ?>

	<div id="page-content" class="seminars">
		<div class="container">
			<div class="content">
				<h2 class="bordered-title colored"><?php post_type_archive_title(); ?></h2>
				<div class="clearfix show-grid"></div>
				<?php $closed = 1; ?>
				<?php if( $closed == 1 ) { ?>

					<div class="row">
						<div class="col-md-10">
							<h3>Breakout Session Registration is now closed.</h3>
						</div>
					</div>

				<?php } else { ?>
					<div class="row">

						<?php if(is_user_logged_in()): ?>

							<div class="col-md-4 col-md-push-8">
								<?php

									if( !empty($user_info) ) {
										$delegates = get_field( 'delegate', $user_info[0]->ID );

										if(isset($_GET['delegate'])){
											$current_delegate = $_GET['delegate'] - 1;
										} else {
											$current_delegate = 0;
										}

										$delegate_count = 1;
										echo '<div class="user-delegates">';
											foreach( $delegates as $delegate ){
												if( $delegate_count == ($current_delegate + 1) ) {
													echo '<div class="col-xs-12"><a href="?delegate='.$delegate_count.'" class="active-user">'.$delegate['title'].' '.$delegate['name_to_appear_in_badge'].'</a></div>';
												} else {
													echo '<div class="col-xs-12"><a href="?delegate='.$delegate_count.'">'.$delegate['title'].' '.$delegate['name_to_appear_in_badge'].'</a></div>';
												}

												$delegate_count++;
											}
										echo '</div>';

										if( !empty( $delegates[$current_delegate]['breakout_session'] ) ):
											$formurl = "/wp-content/themes/mice/includes/generate-reports/pdf-generate/delegate-summary.php";
											echo '<div class="col-xs-12"><a href="'.site_url($formurl).'?user='.$current_user->ID.'&delegate='.$current_delegate.'" class="download-summary-btn">Download Summary</a></div>';
										endif;

										/*echo '<pre>';
											print_r($delegates[$current_delegate]);
										echo '</pre>';*/
									} else {
										$current_delegate = null;
									}

								?>
							</div>

							<div class="col-md-8 col-md-pull-4">

								<?php if( !empty( $delegates[$current_delegate]['breakout_session'] ) ): ?>

									<?php
										$delegate_selections = $delegates[$current_delegate]['breakout_session'];


										if( !empty($delegate_selections) ):
											echo '<h3 class="summary-title">Summary</h3>';
											foreach ($delegate_selections as $delegate_selection) {
												$selection_parent_id = wp_get_post_parent_id( $delegate_selection );

												echo '<div class="clearfix"></div>';

												echo '<p><strong>Topic:</strong> <em>'.get_the_title( $delegate_selection ).'</em></p>';
												echo '<p><strong>Date:</strong> '.get_field( 'date', $selection_parent_id ).'</p>';
												echo '<p><strong>Time:</strong> '.get_field( 'time', $selection_parent_id ).'</p>';

												echo '<div class="clearfix show-grid2"></div>';
											}
										endif;
									?>

								<?php else: ?>

									<?php
										if( isset($_POST['participate']) ):

											$sessions_ids = array();

											wp_reset_postdata();
											$args = array(
												'post_type'			=> 'seminars',
												'posts_per_page'	=> -1,
												'post_parent'		=> 0
											);
											$custom_query = new WP_Query($args);
											if($custom_query->have_posts()):
												while($custom_query->have_posts()): $custom_query->the_post();

													$sessions_ids[] = $_POST['selection_'.$post->ID];

													wp_reset_postdata();
													$courses_arg = array(
														'post_type'			=> 'seminars',
														'posts_per_page'	=> -1,
														'post_parent'		=> $post->ID,
														'orderby'			=> 'id',
														'order'				=> 'ASC'
													);
													$courses = new WP_Query($courses_arg);
													if($courses->have_posts()):
														while($courses->have_posts()): $courses->the_post();
															$checking_count = 0;
															$get_users_checking = array( 'post_type' => 'users', 'posts_per_page' => -1 );
															$get_users_check = get_posts( $get_users_checking );
															foreach ($get_users_check as $get_user_check) {
																$check_user_delegates = get_field( 'delegate', $get_user_check->ID );
																foreach ($check_user_delegates as $check_user_delegate) {
																	if( !empty($check_user_delegate['breakout_session']) ){
																		if( in_array($post->ID, $check_user_delegate['breakout_session']) ){
																			$checking_count++;
																		}
																	}
																}
															}
															$max_participants = get_field( 'max_participants' );
															if( $checking_count >= $max_participants ) {
																$sessions_ids[] = 'false';
															}
														endwhile;
													endif;
													wp_reset_postdata();

												endwhile;
											endif;

											if( isset($_GET['delegate']) ){
												$delegate_to_udate = $_GET['delegate'] - 1;
												$delegate_redirect = $_GET['delegate'];
											} else {
												$delegate_to_udate = 0;
												$delegate_redirect = 1;
											}

											/*echo '<pre>';
												print_r($sessions_ids);
											echo '</pre>';*/

											if( !in_array( 'false', $sessions_ids ) ){
												update_post_meta( $user_info[0]->ID, 'delegate_'.$delegate_to_udate.'_breakout_session', array_filter($sessions_ids) ); ?>
												<script type="text/javascript"> window.location.replace("<?php echo get_bloginfo( 'url' ); ?>/seminars/?delegate=<?php echo $delegate_redirect; ?>"); </script>
											<?php } else {
												echo 'Error saving, max participants reached in 1 of the sessions.';
											}

											wp_reset_postdata();
										endif;
									?>


									<script type="text/javascript">
										$(document).ready(function(){
											$('#submit').click(function(){
												var count = 0;
												var new_count = 0;
												var selected = [];
												$('.participate_field').each(function(){
													if( $(this).prop('checked') ){
														selected[count] = $(this).val();
														count++;
													}
												});

												$('#submit').addClass('disable');

												$.ajax({
													type: "POST",
													url: "<?php bloginfo('template_directory'); ?>/ajax-review.php",
													data: 'selections='+selected,
													success: function(dataSuccess, response){
														if( dataSuccess != 'empty' ){
															$('.summary-details').html( dataSuccess );
															$('input[name="participate"]').attr('disabled', false);
														} else {
															$('.summary-details').html( '<p>No selections to save.</p>' );
														}

														$('.overlay, #summary').fadeIn();

														if( $('#summary').is(":visible") ){
															var summary_width = $('#summary').innerWidth();
															var summary_position = Math.floor(summary_width / 2);

															$('#summary').css({
																'left': '50%',
																'margin-left': '-'+summary_position+'px'
															});
														}

														$('#submit').removeClass('disable');
													}
												}); // end size ajax


												return false;
											});

											$('.close-summary, #close-summary').click(function(){
												$('#summary, .overlay').fadeOut();

												return false;
											});
										});
									</script>

									<form method="post">
										<div class="overlay"></div>
										<div id="summary">
											<span class="glyphicon glyphicon-remove close-summary"></span>
											<h3 class="bordered-title colored">Summary</h3>

											<div class="summary-details"></div>
											<input type="submit" name="participate" class="button button-primary button-large" disabled="" value="Submit">
											<a href="#" id="close-summary" class="button button-primary button-large">Cancel</a>
										</div>

										<div class="breakout-sessions">
											<div class="table-responsive">
												<table class="table table-bordered breakout-session-selection">
													<?php if(have_posts()): ?>
														<?php while(have_posts()): the_post(); ?>
															<tr>
																<th colspan="2"><?php the_title(); ?>: <span class="pull-right"><?php the_field( 'date' ); ?></span></th>
																<?php $parent_id = $post->ID; ?>
															</tr>
																<?php
																	$courses_arg = array(
																		'post_type'			=> 'seminars',
																		'posts_per_page'	=> -1,
																		'post_parent'		=> $post->ID,
																		'orderby'			=> 'id',
																		'order'				=> 'ASC'
																	);
																	$courses = new WP_Query($courses_arg);
																?>

															<?php

																if($courses->have_posts()):
																	$course_count = 1;
																	while($courses->have_posts()): $courses->the_post();

																	$participants_count = 0;
																	$get_users_arg = array( 'post_type' => 'users', 'posts_per_page' => -1 );
																	$get_users = get_posts( $get_users_arg );
																	foreach ($get_users as $get_user) {
																		$user_delegates = get_field( 'delegate', $get_user->ID );
																		foreach ($user_delegates as $user_delegate) {
																			if( !empty($user_delegate['breakout_session']) ){
																				if( in_array($post->ID, $user_delegate['breakout_session']) ){
																					$participants_count++;
																				}
																			}
																		}
																	}

																	?>
																		<tr>
																			<?php if( $course_count == 1 ){ ?>
																				<td class="session-time" rowspan="<?php echo $courses->found_posts; ?>">
																					<?php the_field('time',$parent_id); ?>
																				</td>
																			<?php } ?>
																			<td>
																				<strong>Course <?php echo $course_count; ?>:</strong> <?php the_field( 'course' ); ?><br>
																				<strong>Title:</strong> <?php the_title(); ?><br>
																				<strong>Speaker/s:</strong> <?php the_field( 'speakers' ); ?>
																				<?php if( get_field( 'moderator' ) && get_field( 'moderator' ) != '' ){ ?>
																				<strong>Moderator:</strong> <?php the_field( 'moderator' ); ?>
																				<?php } ?>
																				<strong>Target Participants:</strong> <?php the_field( 'target_participants' ); ?><br>
																				<strong>Attendees:</strong> <?php echo $participants_count; ?>/<?php echo get_field( 'max_participants' ); ?>
																				<br><br>
																				<?php if( $current_delegate !== null ): ?>
																					<?php if( !empty($delegates[$current_delegate]['breakout_session']) ){ ?>
																						<?php if( in_array( $post->ID, $delegates[$current_delegate]['breakout_session'] ) ){ ?>
																							<label><input type="radio" name="selection_<?php echo $parent_id; ?>" value="<?php echo $post->ID; ?>" class="participate_field" checked> Participate</label>
																						<?php } else { ?>
																							<label><input type="radio" name="selection_<?php echo $parent_id; ?>" value="<?php echo $post->ID; ?>" class="participate_field"> Participate</label>
																						<?php } ?>
																					<?php } else { ?>
																						<?php if( $participants_count ==  get_field( 'max_participants' )){ ?>
																						<?php } else { ?>
																							<label><input type="radio" name="selection_<?php echo $parent_id; ?>" value="<?php echo $post->ID; ?>" class="participate_field"> Participate</label>
																						<?php } ?>
																					<?php } ?>
																				<?php endif; ?>
																			</td>

																		</tr>
																	<?php
																	$course_count++;
																	endwhile;
																endif;
																wp_reset_postdata();
															?>

														<?php endwhile; ?>
													<?php endif; ?>
												</table>
											</div>
											<div id="submit">
												<a href="#summary" class="button button-primary button-large">Proceed</a>
											</div>
											<!-- <input type="submit" name="participate" class="button button-primary button-large" value="Submit"> -->
										</div> <!-- END BREAKOUT SESSIONS -->
									</form>

								<?php endif; ?>
							</div>

						<?php else: ?>

							<div class="col-xs-push-3 col-xs-6">
								<div class="micecon-registration-form">
									<div class="row">
										<div class="acf-field col-xs-12">
											<div class="acf-label">
												<label></label>
											</div>
											<div class="acf-input">
												<legend>Login</legend>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-xs-push-3 col-xs-6">
											<?php wp_login_form(); ?>
											<a href="<?php bloginfo( 'url' ); ?>/forgot-password">Forgot Password</a>
										</div>
									</div>
								</div>

								<?php wp_login_form(); ?>
							</div>

						<?php endif; ?>
					</div>

				<?php } ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
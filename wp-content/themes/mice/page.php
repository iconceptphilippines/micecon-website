<?php get_header(); ?>

	<!-- CONTENT -->
	<div id="page-content">
		<div class="container">

			<div class="content">
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
						<h2 class="bordered-title colored show-grid"><?php the_title(); ?></h2>
						<div class="clearfix show-grid"></div>

						<?php the_content(); ?>

						<?php if(have_rows('additional_contents')): ?>
							<?php while(have_rows('additional_contents')): the_row(); ?>
								<div class="additional-content clearfix">
									<div class="col-lg-1">
										<img src="<?php the_sub_field('content_image'); ?>" alt="">
									</div>
									<div class="col-lg-11">
										<?php the_sub_field('content_text'); ?>
									</div>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>

					<?php endwhile; ?>
				<?php endif; ?>
			</div>

		</div>
	</div>
	<!-- END CONTENT -->

<?php get_footer(); ?>
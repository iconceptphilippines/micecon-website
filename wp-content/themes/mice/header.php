<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="icon" type="image/png" href="<?php bloginfo( 'template_url' ); ?>/micecon-favicon.png" />
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,200,600,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700,300' rel='stylesheet' type='text/css'>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<!-- HEADER -->
	<div id="header">
		<div class="container-fluid">
			<div class="row">

				<!-- MENU -->
				<div class="menu-container">
					<?php
						$args = array(
							'theme_location' => 'main_menu',
							'menu' => '',
							'container' => '',
							'container_class' => '',
							'container_id' => '',
							'menu_class' => 'menu',
							'menu_id' => '',
							'echo' => true,
							'fallback_cb' => 'wp_page_menu',
							'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
						);

						wp_nav_menu( $args );
					?>
				</div>
				<!-- END MENU -->

				<!-- BANNER -->
				<?php if(is_home()): ?>
					<div id="banner">
						<?php
							$banner_arg = array(
								'post_type'			=> 'page',
								'pagename'			=> 'banner',
								'posts_per_page'	=> -1
							);
							$banner = new WP_Query($banner_arg);
							if($banner->have_posts()):
								while($banner->have_posts()): $banner->the_post();
						?>
							<div class="swiper-container">
								<div class="swiper-wrapper">
									<?php
										if(have_rows('slider_images')):
											while(have_rows('slider_images')): the_row();
												echo '<div class="swiper-slide"><div class="slide" style="background-image: url('.get_sub_field('slide_image').');"></div></div>';
											endwhile;
										endif;
									?>
								</div>
							</div>
							<div id="banner-overlay">
								<div class="container">

										<div class="col-lg-12">
											<?php // if(have_rows('logos')): ?>
												<!-- <ul id="logos">
													<?php // while(have_rows('logos')): the_row(); ?>
														<li><a href="<?php // echo get_sub_field('logo_link'); ?>"><img src="<?php // echo get_sub_field('logo'); ?>" alt=""></a></li>
													<?php // endwhile; ?>
												</ul> -->
											<?php // endif; ?>
											<div id="logo">
												<?php if(get_header_image() != ''): ?>
													<a href="http://www.phitex.ph/"><img src="<?php bloginfo( 'template_url' ); ?>/images/phitex-logo-j.png" alt=""></a>
													<a href="<?php bloginfo('url'); ?>"><img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /></a>
												<?php else: ?>
													<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/micecon.png" alt=""></a>
												<?php endif; ?>
											</div>

											<div id="banner-content-text">
												<h2 class="text-center"><?php the_field('banner_text'); ?></h2>
												<?php the_field('bannert_text_info'); ?>
												<?php if(have_rows('registration_links')): ?>
													<ul id="registration_links">
														<?php while(have_rows('registration_links')): the_row(); ?>
															<li class="<?php echo get_sub_field('registration_class'); ?>"><a href="<?php echo get_sub_field('registration_link'); ?>"><img src="<?php echo get_sub_field('registration_link_icon'); ?>" alt=""></a></li>
														<?php endwhile; ?>
													</ul>
												<?php endif; ?>
											</div>
										</div>

								</div> <!-- end container -->
								<ul class="banner-logos">
									<li>
										<img src="https://www.phitex.ph/wp-content/uploads/2015/06/souteasth-asia.png" title="souteasth-asia" alt="">
									</li>	
								</ul>
							</div> <!-- end banner overlay -->
							<?php endwhile;
							endif;
						?>
					</div>
				<?php else: ?>
				<div id="image-banner">
						<?php
							$banner_arg = array(
								'post_type'			=> 'page',
								'pagename'			=> 'banner',
								'posts_per_page'	=> -1
							);
							$banner = new WP_Query($banner_arg);
							if($banner->have_posts()):
								while($banner->have_posts()): $banner->the_post(); ?>
									<div class="container">
										<div id="logo">
											<?php if(get_header_image() != ''): ?>
												<a href="<?php bloginfo('url'); ?>"><img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /></a>
											<?php else: ?>
												<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/micecon.png" alt=""></a>
											<?php endif; ?>
										</div>
									</div>
								<?php endwhile;
							endif;
						?>
					</div>
				<?php endif; ?>
				<!-- END BANNER -->

			</div> <!-- END ROW -->
		</div> <!-- END CONTAINER -->
	</div>
	<!-- END HEADER -->

	<!-- CONTENT -->
	<div id="content-area">
		<div class="container-fluid">
			<div class="row">
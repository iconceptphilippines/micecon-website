<?php get_header(); ?>
				
	<!-- CONTENT -->
	<div class="col-lg-8">
		<div class="content">
			<?php if(have_posts()): ?>
				<h2><?php printf( __( 'Search Results for: %s', 'twentyfourteen' ), get_search_query() ); ?></h2>

				<?php while(have_posts()): the_post(); ?>
					<h3><?php the_title(); ?></h3>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php else: ?>
				<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'project_name' ); ?></p>
				<?php get_search_form(); ?>
			<?php endif; ?>
		</div>
	</div>
	<!-- END CONTENT -->

	<!-- SIDEBAR -->
	<div class="col-lg-4">
		<div class="sidebar">
			<?php get_sidebar(); ?>
		</div>
	</div>
	<!-- END SIDEBAR -->

<?php get_footer(); ?>
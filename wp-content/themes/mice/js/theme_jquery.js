function resizeBanner(){
	var h = $(window).innerHeight() - $('.menu-container').height() -2;

	$('.swiper-container').height(h);
	$('.swiper-container .slide').height(h);
}

$(document).ready(function(){
	// Swiper
	var mySwiper = new Swiper ('.swiper-container', {
		// Optional parameters
		direction: 'horizontal',
		loop: true,
		autoplay: 5000,
		effect: 'fade',
		fade: {
		  crossFade: true
		},
		speed: 5000,
		spaceBetween: 5000,
	});

	if(typeof acf !== 'undefined'){
		acf.add_action('add_field_error', function($field) {
		var $error = $field.children('.acf-input').children('.acf-error-message').remove();
			$field.children('.acf-input').append($error);
		});
	}

	$('.clearfix-bottom').after('<div class="clearfix"></div>');
});

$(window).resize(function(){
	resizeBanner();
});

$(window).load(function(){
	resizeBanner();
});
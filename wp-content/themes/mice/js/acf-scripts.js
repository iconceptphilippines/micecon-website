(function( $ ){

	if ( acf ) {
		acf.add_filter('date_picker_args', function(args, $field) {
			// console.log($field.parent());
			var key = $field.data('key');


			// Normal
			/*if ( key == 'field_5594f3581e085' ) {

				args.onSelect = function( dateText ) {
					$input = $field.find('input[type="text"]');
					$elEndDate = $field.parent().find('[data-key="field_5594f3601e086"]').find('input[type="text"]');

					console.log( $elEndDate );

					// console.log(new Date(dateText));
					// $input.datepicker( "option", "minDate", dateText );
					$elEndDate.datepicker( "option", "minDate", dateText );
					// args.minDate = new Date(2015, 07, 02),
				}
			}

			if ( key == 'field_5594f3601e086' ) {

				args.onSelect = function( dateText ) {
					$startDate = $field.parent().find('[data-key="field_5594f3581e085"]').find('input[type="text"]');

					// console.log(new Date(dateText));
					// $input.datepicker( "option", "minDate", dateText );
					$startDate.datepicker( "option", "maxDate", dateText );
					// args.minDate = new Date(2015, 07, 02),
				}
			}*/

			// Repeater
			if ( key == 'field_55939b721972d' ) {

				args.onSelect = function( dateText ) {
					$input = $field.find('input[type="text"]');
					$elEndDate = $field.parent().find('[data-key="field_5593a35c80067"]').find('input[type="text"]');

					// console.log( $elEndDate );
					$elEndDate.datepicker( "option", "minDate", dateText );
				}
			}

			if ( key == 'field_5593a35c80067' ) {

				args.onSelect = function( dateText ) {
					$startDate = $field.parent().find('[data-key="field_55939b721972d"]').find('input[type="text"]');
					$startDate.datepicker( "option", "maxDate", dateText );
				}
			}

			if ( key == 'field_559a06a5a6702' ) {

				args.onSelect = function( dateText ) {
					$input = $field.find('input[type="text"]');
					$elEndDate = $field.parent().find('[data-key="field_559a06c0a6703"]').find('input[type="text"]');

					// console.log( $elEndDate );
					$elEndDate.datepicker( "option", "minDate", dateText );
				}
			}

			if ( key == 'field_559a06c0a6703' ) {

				args.onSelect = function( dateText ) {
					$startDate = $field.parent().find('[data-key="field_559a06a5a6702"]').find('input[type="text"]');
					$startDate.datepicker( "option", "maxDate", dateText );
				}
			}

			return args;
		});

		var row_count = 0;
		var accomp_count = 0;
		var delegate_price_per_person = 0;
		var accompanying_price_per_person = 0;
		var micedelegate_price_per_person = 0;
		var micecodelegate_price_per_person = 0;
		var micedelegate_count = 0;
		var micecodelegate_count = 0;
		var micedelegate_subtotal = 0;
		var micecodelegate_subtotal = 0;

		var price_delegate_sub = 0;
		var ap_price = 0;
		var price_total = 0;
		var country = '';

		var append_count = 0;

		// document ready
		acf.add_action('ready', function( $el ){

			/*$el.find('div[data-key="field_559a20035f7b7"]').after('<div class="col-xs-12 acf-field"><div id="payment-summary" class="table-responsive"><table class="table"><thead>'
								+'<tr>'
									+'<th>Description</th>'
									+'<th>Quantity</th>'
									+'<th>Price</th>'
									+'<th>Subtotal</th>'
								+'</tr>'
							+'</thead>'
							+'<tbody>'
								+'<tr id="price_empty_row">'
									+'<td colspan="4"><em>Please add at least one delegate</em></td>'
								+'</tr>'
								+'<tr id="price_details_delegate">'
									+'<td>Delegate</td>'
									+'<td id="price_delegate_num">0</td>'
									+'<td id="price_delegate_single">8000</td>'
									+'<td id="price_delegate_sub">0</td>'
								+'</tr>'
								+'<tr id="price_details_ap">'
									+'<td>Accompanying Person</td>'
									+'<td id="price_ap_num">0</td>'
									+'<td id="price_ap_single">4000</td>'
									+'<td id="price_ap_sub">0</td>'
								+'</tr>'
								+'<tr id="price_details_mice">'
									+'<td>MICE Mart - Delegate</td>'
									+'<td id="price_mice_num">0</td>'
									+'<td id="price_mice_single">3000</td>'
									+'<td id="price_mice_sub">0</td>'
								+'</tr>'
								+'<tr id="price_details_mice_co">'
									+'<td>MICE Mart - Co-delegate</td>'
									+'<td id="price_mice_co_num">0</td>'
									+'<td id="price_mice_co_single">750</td>'
									+'<td id="price_mice_co_sub">0</td>'
								+'</tr>'
								+'<tr id="price_total_row" class="success">'
									+'<th>Total</th>'
									+'<td></td>'
									+'<td class="text-right currency_code" style="font-weight:bold;"></td>'
									+'<th id="price_total">0</th>'
								+'</tr>'
							+'</tbody></table></div></div>');
*/

			country = $el.find('select[name="acf[field_55920304704b3]"] option:selected').val();

			if($el.find('select[name="acf[field_55920304704b3]"] option:selected') == "Philippines"){
				if( row_count >= 5 ) {
					delegate_price_per_person = 7200;
					$('#price_delegate_single').text(delegate_price_per_person);
					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
				else if( row_count == 3 || row_count == 4 ) {
					delegate_price_per_person = 7500;
					$('#price_delegate_single').text(delegate_price_per_person);
					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
				else {
					delegate_price_per_person = 8000;
					$('#price_delegate_single').text(delegate_price_per_person);
					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}

				accompanying_price_per_person = 4000;
				$('#price_ap_single').text(accompanying_price_per_person);

				ap_price = accomp_count * accompanying_price_per_person;
				$('#price_ap_sub').text(ap_price);

				price_total = price_delegate_sub + ap_price;
				$('#price_total').text(price_total);

				$('.currency_code').text('PHP');
			} else {
				delegate_price_per_person = 200;
				$('#price_delegate_single').text(delegate_price_per_person);
				price_delegate_sub = row_count * delegate_price_per_person;
				$('#price_delegate_sub').text(price_delegate_sub);

				accompanying_price_per_person = 100;
				$('#price_ap_single').text(accompanying_price_per_person);

				ap_price = accomp_count * accompanying_price_per_person;
				$('#price_ap_sub').text(ap_price);

				price_total = price_delegate_sub + ap_price;
				$('#price_total').text(price_total);
				$('.currency_code').text('USD');
				// micedelegate_price_per_person = 0;
				// micecodelegate_price_per_person = 0;
			}

			$el.find('select[name="acf[field_55920304704b3]"]').change(function(){
				// alert($(this).val());
				if($(this).val() != 0){
					country = $(this).val();
				} else {
					country = $(this).val();
				}

				if($(this).val() == "Philippines"){
					if( row_count >= 5 ) {
						delegate_price_per_person = 7200;
						$('#price_delegate_single').text(delegate_price_per_person);
						price_delegate_sub = row_count * delegate_price_per_person;
						$('#price_delegate_sub').text(price_delegate_sub);
					}
					else if( row_count == 3 || row_count == 4 ) {
						delegate_price_per_person = 7500;
						$('#price_delegate_single').text(delegate_price_per_person);
						price_delegate_sub = row_count * delegate_price_per_person;
						$('#price_delegate_sub').text(price_delegate_sub);
					}
					else {
						delegate_price_per_person = 8000;
						$('#price_delegate_single').text(delegate_price_per_person);
						price_delegate_sub = row_count * delegate_price_per_person;
						$('#price_delegate_sub').text(price_delegate_sub);
					}

					accompanying_price_per_person = 4000;
					$('#price_ap_single').text(accompanying_price_per_person);

					ap_price = accomp_count * accompanying_price_per_person;
					$('#price_ap_sub').text(ap_price);

					price_total = price_delegate_sub + ap_price;
					$('#price_total').text(price_total);

					$('.currency_code').text('PHP');
					// micedelegate_price_per_person = 3000;
					// micecodelegate_price_per_person = 750;
				} else {
					delegate_price_per_person = 200;
					$('#price_delegate_single').text(delegate_price_per_person);
					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);

					accompanying_price_per_person = 100;
					$('#price_ap_single').text(accompanying_price_per_person);

					ap_price = accomp_count * accompanying_price_per_person;
					$('#price_ap_sub').text(ap_price);

					price_total = price_delegate_sub + ap_price;
					$('#price_total').text(price_total);

					$('.currency_code').text('USD');
				}
			});

			$('#price_details_delegate, #price_details_ap, #price_total_row, #price_details_mice, #price_details_mice_co').hide();

			if($('div[data-name="delegate"]').find('.acf-table > tbody > tr').length > 1){
				row_count = $('div[data-name="delegate"]').find('.acf-table > tbody > tr').length - 1;

				if(row_count > 0){
					$('#price_details_delegate, #price_details_ap, #price_total_row').show();
					$('#price_empty_row').hide();
					$('#price_delegate_num').text(row_count);
				} else {
					$('#price_details_delegate, #price_details_ap, #price_total_row').hide();
					$('#price_empty_row').show();
					$('#price_delegate_num').text(row_count);
				}

				if(country == "Philippines"){
					if( row_count >= 5 ) {
						delegate_price_per_person = 7200;
						$('#price_delegate_single').text(delegate_price_per_person);

						price_delegate_sub = row_count * delegate_price_per_person;
						$('#price_delegate_sub').text(price_delegate_sub);
					}
					else if( row_count == 3 || row_count == 4 ) {
						delegate_price_per_person = 7500;
						$('#price_delegate_single').text(delegate_price_per_person);

						price_delegate_sub = row_count * delegate_price_per_person;
						$('#price_delegate_sub').text(price_delegate_sub);
					}
					else {
						delegate_price_per_person = 8000;
						$('#price_delegate_single').text(delegate_price_per_person);

						price_delegate_sub = row_count * delegate_price_per_person;
						$('#price_delegate_sub').text(price_delegate_sub);
					}
					price_total = price_delegate_sub + ap_price;
					$('#price_total').text(price_total);
				} else {
					delegate_price_per_person = 200;

					if( row_count >= 5 ) {
						price_delegate_sub = row_count * delegate_price_per_person;
						$('#price_delegate_sub').text(price_delegate_sub);
					}
					else if( row_count == 3 || row_count == 4 ) {
						price_delegate_sub = row_count * delegate_price_per_person;
						$('#price_delegate_sub').text(price_delegate_sub);
					}
					else {
						price_delegate_sub = row_count * delegate_price_per_person;
						$('#price_delegate_sub').text(price_delegate_sub);
					}
					price_total = price_delegate_sub + ap_price;
					$('#price_total').text(price_total);
				}

				if($('div[data-key="field_559a016c50bda"]').find('input[type="checkbox"]').attr('checked', true).length > 1) {
					accomp_count = $('div[data-key="field_559a016c50bda"]').find('input[type="checkbox"]').attr('checked', true).length - 1;
					$('#price_ap_num').text(accomp_count);

					if(country == 'Philippines'){
						accompanying_price_per_person = 4000;
						$('#price_ap_single').text(accompanying_price_per_person);

						ap_price = accomp_count * accompanying_price_per_person;
						$('#price_ap_sub').text(ap_price);
					} else {
						accompanying_price_per_person = 100;
						$('#price_ap_single').text(accompanying_price_per_person);

						ap_price = accomp_count * accompanying_price_per_person;
						$('#price_ap_sub').text(ap_price);
					}

					price_total = price_delegate_sub + ap_price;
					$('#price_total').text(price_total);
				}
			}

		});


		// show conditional hidden fields
		acf.add_action('show_field', function( $field, context ){
			var key = $field.data('key');
			if(key == 'field_559a0323763af'){
				$field.find('input[type="text"]').attr('disabled', true);
			}
		});


		// Add Row
		acf.add_action('append', function( $el ){
			row_count++;
			$('#price_delegate_num').text(row_count);

			if(row_count > 0){
				$('#price_details_delegate, #price_details_ap, #price_total_row').show();
				$('#price_empty_row').hide();
			} else {
				$('#price_details_delegate, #price_details_ap, #price_total_row').hide();
				$('#price_empty_row').show();
			}

			if(country == "Philippines"){
				if( row_count >= 5 ) {
					delegate_price_per_person = 7200;
					$('#price_delegate_single').text(delegate_price_per_person);

					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
				else if( row_count == 3 || row_count == 4 ) {
					delegate_price_per_person = 7500;
					$('#price_delegate_single').text(delegate_price_per_person);

					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
				else {
					delegate_price_per_person = 8000;
					$('#price_delegate_single').text(delegate_price_per_person);

					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
			} else {
				delegate_price_per_person = 200;

				if( row_count >= 5 ) {
					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
				else if( row_count == 3 || row_count == 4 ) {
					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
				else {
					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
			}

			price_total = micedelegate_subtotal + micecodelegate_subtotal + price_delegate_sub + ap_price;
			$('#price_total').text(price_total);

			$el.find('div[data-key="field_559a016c50bda"]').find('input[type="checkbox"]').change(function(){
				if($(this).prop('checked')){
					accomp_count++;
					$('#price_ap_num').text(accomp_count);


					if(country == 'Philippines'){
						accompanying_price_per_person = 4000;
						$('#price_ap_single').text(accompanying_price_per_person);

						ap_price = accomp_count * accompanying_price_per_person;
						$('#price_ap_sub').text(ap_price);
					} else {
						accompanying_price_per_person = 100;
						$('#price_ap_single').text(accompanying_price_per_person);

						ap_price = accomp_count * accompanying_price_per_person;
						$('#price_ap_sub').text(ap_price);
					}

					price_total = micedelegate_subtotal + micecodelegate_subtotal + price_delegate_sub + ap_price;
					$('#price_total').text(price_total);
				}
				if(!$(this).prop('checked')){
					accomp_count--;
					$('#price_ap_num').text(accomp_count);

					ap_price = accomp_count * accompanying_price_per_person;
					$('#price_ap_sub').text(ap_price);

					price_total = micedelegate_subtotal + micecodelegate_subtotal + price_delegate_sub + ap_price;
					$('#price_total').text(price_total);
				}
			});


			// var badge_field = $('div[data-name="name_to_appear_in_badge"]', $el).find('input[type="text"]').attr('disabled',true);
			var badge_field = $('div[data-name="name_to_appear_in_badge"]', $el).find('input[type="text"]');
			badge_field.val("(Auto-Generated after entering your Nickname and Last Name)");

			var x = $el.find('div[data-key="field_559341c0726c1"]').find('input[type="text"]').val();
			var y = $el.find('div[data-key="field_559341b7726c0"]').find('input[type="text"]').val();

			$('#acf-field_559b3aba305e7').append('<option class="delegate_'+row_count+'">'+x+' '+y+'</option>');
			$('#acf-field_559b3b0b305e8').append('<option class="delegate_'+row_count+'">'+x+' '+y+'</option>');
		});

		acf.add_action('load', function( $el ){
			var check_admin = $('input[name="_acfobjectid"]').val();
			// alert(check_admin);
			if(check_admin == '' || check_admin == 'undefined' || check_admin == undefined){
				// var badge_field = $('div[data-name="name_to_appear_in_badge"]', $el).find('input[type="text"]').attr('disabled',true);
				var badge_field = $('div[data-name="name_to_appear_in_badge"]', $el).find('input[type="text"]');
				badge_field.val("(Auto-Generated after entering your Nickname and Last Name)");

				var x = $el.find('div[data-key="field_559341c0726c1"]').find('input[type="text"]').val();
				var y = $el.find('div[data-key="field_559341b7726c0"]').find('input[type="text"]').val();

				$('#acf-field_559b3aba305e7').append('<option class="delegate_'+row_count+'">'+x+' '+y+'</option>');
				$('#acf-field_559b3b0b305e8').append('<option class="delegate_'+row_count+'">'+x+' '+y+'</option>');
			}

			$(document).on("keyup keypress blur", '.acf-field[data-name="nickname"] input', function(){
				append_count = $(this).parent().parent().parent().parent().parent().index() + 1;
				var nickname = $(this).val();
				var lastname = $(this).parent().parent().parent().parent().find(".acf-field[data-name='last_name']").find('input[type="text"]').val();
				$(this).parent().parent().parent().parent().find('div[data-name="name_to_appear_in_badge"]').find('input[type="text"]').val(nickname+' '+lastname);
				$('option.delegate_'+append_count).text(nickname+' '+lastname);
				if (nickname == "" && lastname == "") {
					$(this).parent().parent().parent().parent().find('div[data-name="name_to_appear_in_badge"]').find('input[type="text"]').val('(Auto-Generated after entering your Nickname and Last Name)');
				}
			});

			$(document).on("keyup keypress blur", '.acf-field[data-name="last_name"] input', function(){
				append_count = $(this).parent().parent().parent().parent().parent().index() + 1;
				var nickname = $(this).parent().parent().parent().parent().find(".acf-field[data-name='nickname']").find('input[type="text"]').val();
				var lastname = $(this).val();
				$(this).parent().parent().parent().parent().find('div[data-name="name_to_appear_in_badge"]').find('input[type="text"]').val(nickname+' '+lastname);
				$('option.delegate_'+append_count).text(nickname+' '+lastname);
				if (nickname == "" && lastname == "") {
					$(this).parent().parent().parent().parent().find('div[data-name="name_to_appear_in_badge"]').find('input[type="text"]').val('(Auto-Generated after entering your Nickname and Last Name)');
				}
			});

			if(check_admin != '' || check_admin != 'undefined' || check_admin != undefined){
				var micemart_del_count = 1;
				$('#acf-field_559b3aba305e7 option').each(function(){
					if($(this).val() != '0'){
						$(this).addClass('delegate_'+micemart_del_count);
						micemart_del_count++
					}
				});
				var micemart_co_del_count = 1;
				$('#acf-field_559b3b0b305e8 option').each(function(){
					if($(this).val() != '0'){
						$(this).addClass('delegate_'+micemart_co_del_count);
						micemart_co_del_count++;
					}
				});

				var delegeate_selected = $('#acf-field_559b3aba305e7 option:selected').attr('class');
				var co_delegeate_selected = $('#acf-field_559b3b0b305e8 option:selected').attr('class');
				$('#acf-field_559b3aba305e7 option.'+co_delegeate_selected).attr('disabled', true);
				$('#acf-field_559b3b0b305e8 option.'+delegeate_selected).attr('disabled', true);
			}
			$('#acf-field_559b3aba305e7').change(function(){
				if(country == "Philippines"){
					micedelegate_price_per_person = 3000;
					micecodelegate_price_per_person = 750;
				} else {
					micedelegate_price_per_person = 0;
					micecodelegate_price_per_person = 0;
				}
				if($('option:selected', $(this)).val() != 0){
					// $('#price_details_mice').show();

					var get_class = $('option:selected', $(this)).attr('class');

					//$('option', $(this)).removeAttr('disabled');
					$('#acf-field_559b3b0b305e8 option').removeAttr('disabled');

					var co_del = $('#acf-field_559b3b0b305e8 option:selected').attr('class');
					$('option.'+co_del, $(this)).attr('disabled', true);
					// $('option.'+get_class, $(this)).attr('disabled', true);
					$('#acf-field_559b3b0b305e8 option.'+get_class).attr('disabled', true);
					//$('#acf-field_559b3b0b305e8 option.'+co_del).attr('disabled', true);

					micedelegate_count++;
					$('#price_mice_num').text(micedelegate_count);
					$('#price_mice_single').text(micedelegate_price_per_person);
					micedelegate_subtotal = micedelegate_count * micedelegate_price_per_person;
					$('#price_mice_sub').text(micedelegate_subtotal);
				} else {
					// $('#price_details_mice').hide();
					$('option', $(this)).removeAttr('disabled');
					$('#acf-field_559b3b0b305e8 option').removeAttr('disabled');

					var co_del = $('#acf-field_559b3b0b305e8 option:selected').attr('class');
					$('option.'+co_del, $(this)).attr('disabled', true);
					// $('#acf-field_559b3b0b305e8 option.'+co_del).attr('disabled', true);

					micedelegate_count--;
					$('#price_mice_num').text(micedelegate_count);
					$('#price_mice_single').text(micedelegate_price_per_person);
					micedelegate_subtotal = micedelegate_count * micedelegate_price_per_person;
					$('#price_mice_sub').text(micedelegate_subtotal);
				}
				price_total = micedelegate_subtotal + micecodelegate_subtotal + price_delegate_sub + ap_price;
				$('#price_total').text(price_total);
			});

			$('#acf-field_559b3b0b305e8').change(function(){
				if(country == "Philippines"){
					micedelegate_price_per_person = 3000;
					micecodelegate_price_per_person = 750;
				} else {
					micedelegate_price_per_person = 0;
					micecodelegate_price_per_person = 0;
				}
				if($('option:selected', $(this)).val() != 0){
					// $('#price_details_mice_co').show();

					var get_class = $('option:selected', $(this)).attr('class');

					// $('option', $(this)).removeAttr('disabled');
					$('#acf-field_559b3aba305e7 option').removeAttr('disabled');

					var co_del = $('#acf-field_559b3aba305e7 option:selected').attr('class');
					$('option.'+co_del, $(this)).attr('disabled', true);
					// $('option.'+get_class, $(this)).attr('disabled', true);
					$('#acf-field_559b3aba305e7 option.'+get_class).attr('disabled', true);
					// $('#acf-field_559b3aba305e7 option.'+co_del).attr('disabled', true);

					micecodelegate_count++;
					$('#price_mice_co_num').text(micecodelegate_count);
					$('#price_mice_co_single').text(micecodelegate_price_per_person);
					micecodelegate_subtotal = micecodelegate_count * micecodelegate_price_per_person;
					$('#price_mice_co_sub').text(micecodelegate_subtotal);
				} else {
					// $('#price_details_mice_co').hide();
					$('option', $(this)).removeAttr('disabled');
					$('#acf-field_559b3aba305e7 option').removeAttr('disabled');

					var co_del = $('#acf-field_559b3aba305e7 option:selected').attr('class');
					$('option.'+co_del, $(this)).attr('disabled', true);
					// $('#acf-field_559b3aba305e7 option.'+co_del).attr('disabled', true);
					micecodelegate_count--;
					$('#price_mice_co_num').text(micecodelegate_count);
					$('#price_mice_co_single').text(micecodelegate_price_per_person);
					micecodelegate_subtotal = micecodelegate_count * micecodelegate_price_per_person;
					$('#price_mice_co_sub').text(micecodelegate_subtotal);
				}

				price_total = micedelegate_subtotal + micecodelegate_subtotal + price_delegate_sub + ap_price;
				$('#price_total').text(price_total);

			});

		});


		// Remove row
		acf.add_action('remove', function( $el ){
			append_count = $($el).index() + 1;
			$('#acf-field_559b3aba305e7 option.delegate_'+append_count).remove();
			$('#acf-field_559b3b0b305e8 option.delegate_'+append_count).remove();

			row_count--;
			$('#price_delegate_num').text(row_count);

			if(row_count > 0){
				$('#price_details_delegate, #price_details_ap, #price_total_row').show();
				$('#price_empty_row').hide();
			} else {
				$('#price_details_delegate, #price_details_ap, #price_total_row').hide();
				$('#price_empty_row').show();
			}

			if(country == "Philippines"){
				if( row_count >= 5 ) {
					delegate_price_per_person = 7200;
					$('#price_delegate_single').text(delegate_price_per_person);

					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
				else if( row_count == 3 || row_count == 4 ) {
					delegate_price_per_person = 7500;
					$('#price_delegate_single').text(delegate_price_per_person);

					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
				else {
					delegate_price_per_person = 8000;
					$('#price_delegate_single').text(delegate_price_per_person);

					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
			} else {
				delegate_price_per_person = 200;

				if( row_count >= 5 ) {
					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
				else if( row_count == 3 || row_count == 4 ) {
					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
				else {
					price_delegate_sub = row_count * delegate_price_per_person;
					$('#price_delegate_sub').text(price_delegate_sub);
				}
			}

			price_total = micedelegate_subtotal + micecodelegate_subtotal + price_delegate_sub + ap_price;
			$('#price_total').text(price_total);

			if( $el.find('div[data-key="field_559a016c50bda"]').find('input[type="checkbox"]') ){
				if($(this).prop('checked')){
					if(accomp_count > 0){
						accomp_count--;
					}
					$('#price_ap_num').text(accomp_count);


					if(country == 'Philippines'){
						accompanying_price_per_person = 4000;
						$('#price_ap_single').text(accompanying_price_per_person);

						ap_price = accomp_count * accompanying_price_per_person;
						$('#price_ap_sub').text(ap_price);
					} else {
						accompanying_price_per_person = 100;
						$('#price_ap_single').text(accompanying_price_per_person);

						ap_price = accomp_count * accompanying_price_per_person;
						$('#price_ap_sub').text(ap_price);
					}

					price_total = micedelegate_subtotal + micecodelegate_subtotal + price_delegate_sub + ap_price;
					$('#price_total').text(price_total);
				}
			}
		});

	}
})(jQuery);

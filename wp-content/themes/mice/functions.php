<?php
/**
 * Project Name Theme
 *
 * This custom wordpress theme was designed and developed by iConcept Global Advertising Inc.
 * Every element found in the layout of the theme and the functionality was checked and approved by the website owner.
 * It was based on what kind of website the owner wants and what functionality of his/her website is.
 *
 **/
// Replace "micecon" to the project you are going.

if ( ! function_exists( 'micecon_setup' ) ) :
	function micecon_setup(){

		// Enable support for Post Thumbnails, and declare two sizes.
		add_theme_support( 'post-thumbnails' );

		// Menu locations set in backend
		register_nav_menus( array(
			'main_menu'   => __( 'Main Menu', 'micecon' ),
			'supporting_menu' => __( 'Additional Menu', 'micecon' ),
		) );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
		) );

		// This theme uses its own gallery styles.
		add_filter( 'use_default_gallery_style', '__return_false' );
	}

	/**
	 * Header image
	 **/
	add_theme_support( 'custom-header', apply_filters( 'micecon_custom_header_args', array(
		'default-text-color'     => 'fff',
		'width'                  => 200,
		'flex-width'			 => true,
		'height'                 => 200,
		'flex-height'            => true,
	) ) );

	/**
	 * Custom Background
	 */
	$defaults = array(
	'default-color'          => '',
	'default-image'          => '',
	'wp-head-callback'       => '_custom_background_cb',
	'admin-head-callback'    => '',
	'admin-preview-callback' => ''
	);
	add_theme_support( 'custom-background', $defaults );
endif;
add_action( 'after_setup_theme', 'micecon_setup' );


/**
 * Register micecon widget areas
 **/
function micecon_widgets_init() {
	require get_template_directory() . '/includes/widgets.php';
	register_widget( 'micecon_Ephemera_Widget' );

	register_sidebar( array(
		'name'          => __( 'Main Sidebar', 'micecon' ),
		'id'            => 'sidebar-1',
		'description'   => __( '', 'micecon' ),
		'class'			=> '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Content Sidebar', 'micecon' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Additional sidebar that appears on the right.', 'micecon' ),
		'class'			=> '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'micecon' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears in the footer section of the site.', 'micecon' ),
		'class'			=> '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'micecon_widgets_init' );


/**
 * Include default scripts and css
 * Include in wp_header()
 **/
function include_scripts() {
	/*
	 * Register/Hook Styles
	 */
	wp_enqueue_style('bootstrap_css',get_stylesheet_directory_uri().'/css/bootstrap/bootstrap.min.css');
	wp_enqueue_style('swiper_style',get_stylesheet_directory_uri().'/css/swiper.css');
	wp_enqueue_style('stylesheet',get_stylesheet_uri());

	/*
	 * Register scripts
	 */
	wp_enqueue_script( 'jquery_js', get_template_directory_uri().'/js/jquery-1.11.0.min.js' );
	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() .'/js/bootstrap/bootstrap.min.js' );
	wp_enqueue_script( 'swipper_script', get_template_directory_uri() .'/js/swiper.js' );
	wp_enqueue_script( 'template_scripts', get_template_directory_uri().'/js/theme_jquery.js' );
}
add_action('wp_enqueue_scripts','include_scripts');


/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Index views.
 * 3. Single views.
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function micecon_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if ( is_archive() || is_search() || is_home() ) {
		$classes[] = 'list-view';
	}

	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}

	return $classes;
}
add_filter( 'body_class', 'micecon_body_classes' );


/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function micecon_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'micecon' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'micecon_wp_title', 10, 2 );


/**
 * Include custom functions and scripts
 **/
include( 'includes/functions_custom.php' );
include( 'includes/functions_scripts.php' );


/**
 * Custom Post type Registration
 *
 * Ex. include('includes/post_type/functions_post_type.php');
 **/
include('includes/post_type/functions-news.php');
include('includes/post_type/functions-users.php');
include('includes/post_type/functions-seminars.php');
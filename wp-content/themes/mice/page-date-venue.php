<?php
/*
 * Template Name: Date and Venue
 */
get_header(); ?>

	<!-- CONTENT -->
	<div id="page-content">
		<div class="container">

			<div class="content">
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
					<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
						<div id="event">
							<div class="show-grid2 clearfix"></div>
							<div class="container">
								<h2 class="bordered-title colored show-grid"><?php the_title(); ?></h2>
								<p class="show-grid2"><img src="<?php the_field('event_location_image'); ?>" class="img-responsive center-block" alt=""></p>
								<div class="row clearfix">
									<div class="col-md-4">
										<?php
											if(have_rows('event_details')):
												echo '<ul class="event-details">';
												while(have_rows('event_details')): the_row();
													echo '<li><span class="detail-icon"><img src="'.get_sub_field('detail_icon').'" alt=""></span><span class="detail-text">'.get_sub_field('details').'</span></li>';
												endwhile;
												echo '</ul>';
											endif;
										?>
									</div>
									<div class="col-md-8">
										<div class="event-location">
											<?php the_field('event_location'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; wp_reset_postdata(); ?>
			</div>

		</div>
	</div>
	<!-- END CONTENT -->

<?php get_footer(); ?>
<?php get_header(); ?>

	<div id="page-content" class="single-page">
		<div class="container">
			<div class="content">
				<?php $object = get_queried_object(); ?>
				<h2 class="bordered-title colored"><?php echo $object->post_type; ?></h2>
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
						<h4 class="single-news-title"><?php the_title(); ?></h4>
						<span class="news-meta"><?php the_time('F j, Y'); ?></span>
						<div class="clearfix show-grid2"></div>

						<?php the_post_thumbnail( 'full', array('class'=>'img-responsive img-thumbnail alignleft') ); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
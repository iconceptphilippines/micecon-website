<?php get_header(); ?>

	<div id="page-content" class="news-archive">
		<div class="container">
			<div class="content">
				<h2 class="bordered-title colored"><?php post_type_archive_title(); ?></h2>
				<div class="clearfix show-grid"></div>

				<div class="row">
					<div class="list-news">
						<?php if(have_posts()): ?>
							<?php while(have_posts()): the_post(); ?>
								<div class="col-sm-6 show-grid2 custom-display">
									<div class="news">
										<?php the_post_thumbnail( 'thumbnail', array('class' => 'item-thumbnail') ); ?>
										<span class="news-meta"><?php the_field('news_date'); ?></span>
										<h4 class="list-title"><?php the_title(); ?></h4>
										<p class="list-content"><?php echo short_text(155, get_the_content()); ?></p>
										<p><a href="<?php the_permalink(); ?>">read more</a></p>
									</div>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
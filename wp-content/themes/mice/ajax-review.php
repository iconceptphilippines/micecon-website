<?php

define('WP_USE_THEMES', false);
require('../../../wp-blog-header.php');

if( isset($_POST['selections']) ):
	$selections = explode( ',',$_POST['selections'] );
	$selections = array_filter($selections);
	if( !empty( $selections ) ){

		foreach ($selections as $selection) {
			$parent_id = wp_get_post_parent_id( $selection );

			echo '<div class="clearfix"></div>';

			echo '<p><strong>Topic:</strong> <em>'.get_the_title( $selection ).'</em></p>';
			echo '<p><strong>Date:</strong> '.get_field( 'date', $parent_id ).'</p>';
			echo '<p><strong>Time:</strong> '.get_field( 'time', $parent_id ).'</p>';

			echo '<div class="clearfix show-grid2"></div>';
		}
	} else {
		echo 'empty';
	}
endif;
<?php
/*
 * Template Name: Forgot Password
 *
 */

get_header(); ?>

	<!-- CONTENT -->
	<div id="page-content">
		<div class="container">

			<div class="content">
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
						<h2 class="bordered-title colored show-grid"><?php the_title(); ?></h2>
						<div class="clearfix show-grid"></div>

						<div class="col-xs-push-3 col-xs-6">
							<?php
								if( isset($_POST['email_check']) ){

									$email = $_POST['email_check'];

									if( email_exists( $email ) ){
										$user = get_user_by( 'email', $email );
										$args = array(
											'post_type'	=> 'users',
											'author' => $user->ID
										);
										$post = get_posts( $args );

										$to = $email;
										// $to = 'dexter.chua@iconcept.com.ph';
										$from = get_option('admin_email');
										$subject = 'M.I.C.E. Con Forgot Password';
										$message = 'Username: '.get_field( 'username', $post[0]->ID ).'<br>';
										$message .= 'Password: '.get_field( 'password', $post[0]->ID ).'<br>';
										$headers  = 'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										$headers .= "From:" . $from;
										if( wp_mail( $to, $subject, $message, $headers ) ){
											echo '<p class="bg-success forgotpass-warning">Email sent.</p>';
										}
									} else {
										echo '<p class="bg-warning forgotpass-warning">Incorrect Email Address.</p>';
									}
								}
							?>
							<form action="" class="micecon-forgotpass-form" method="post">
								<input type="text" name="email_check" id="email-check" class="form-control" placeholder="Email Address">
								<div class="clearfix show-grid"></div>
								<div class="acf-form-submit"><input type="submit" name="submit" class="button button-primary button-large"></div>
							</form>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>

		</div>
	</div>

<?php get_footer(); ?>
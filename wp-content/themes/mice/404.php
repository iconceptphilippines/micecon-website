<?php get_header(); ?>
	<div id="page-content">
		<div class="container">

			<div class="content">
				<div class="col-xs-12">
					<div class="clearfix show-grid"></div>
					<h2 class="text-center">Error 404: Page not found.</h2>
					<div class="clearfix show-grid"></div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
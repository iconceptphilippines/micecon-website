<?php
/*
 * Template Name: User Profile Page
 *
 */

get_header(); ?>

	<!-- CONTENT -->
	<div id="page-content">
		<div class="container">

			<div class="content">
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
						<h2 class="bordered-title colored show-grid"><?php the_title(); ?></h2>
						<div class="clearfix show-grid"></div>

						<div class="col-sm-10">
							<div class="row">
								<?php the_content(); ?>
							</div>
						</div>
					<?php if ( is_user_logged_in() ) {
						global $current_user;
      					get_currentuserinfo();
      					$username = $current_user->user_login;

      					$args = array(
							'post_type'		=> 'users',
							'meta_key'		=> 'username',
							'order'			=> 'ASC',
							'meta_query'	=> array(
								array(
									'key'     => 'username',
									'value'   => array($username),
									'compare' => 'IN',
								),
							),
						);
						$query = new WP_Query( $args );
						if( $query->have_posts()) {
							while ( $query->have_posts() ) {
								$query->the_post();
								$postid = get_the_ID();
								$classificationcategory = get_field('classificationcategory', $postid);
								$company_name 			= get_field('company_name', $postid);
								$country 				= get_field('country', $postid);
								$province 				= get_field('province', $postid);
								$city 					= get_field('city', $postid);
								$telephone_no			= array_filter(get_field('telephone_no', $postid));
								$fax_no 				= array_filter(get_field('fax_no', $postid));
								$email 					= get_field('email', $postid);
								$website 				= get_field('website', $postid);
								$company_address 		= get_field('company_address', $postid);
								$delegates 				= get_field('delegate', $postid);
					?>
							<div class="col-sm-10">
								<div class="row">
									<div class="micecon-registration-form">
										<div class="row">
											<div class="acf-field col-xs-12">
												<div class="acf-label">
													<label></label>
												</div>
												<div class="acf-input">
													<legend>Company Profile</legend>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-12">
												<label for="">Classification/Category</label>
												<p><?php echo $classificationcategory; ?></p>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-6">
												<label for="">Company Name</label>
												<p><?php echo $company_name; ?></p>
											</div>
											<div class="col-xs-6">
												<label for="">Country</label>
												<p><?php echo $country; ?></p>
											</div>
										</div>

										<?php
										if($country == "Philippines") { ?>

										<div class="row">
											<div class="col-xs-6">
												<label for="">Province</label>
												<p><?php echo $province; ?></p>
											</div>
											<div class="col-xs-6">
												<label for="">City</label>
												<p><?php echo $city; ?></p>
											</div>
										</div>

										<?php } ?>

										<div class="row">
											<div class="col-xs-6">
												<label for="">Telephone No</label>
												<?php if (!empty($telephone_no)) { ?>
													<p><?php echo $telephone_no['code'] . '-' . $telephone_no['area'] . '-' . $telephone_no['num']; ?></p>
												<?php } ?>
											</div>
											<div class="col-xs-6">
												<label for="">Fax No</label>
												<?php if (!empty($fax_no)) { ?>
													<p><?php echo $fax_no['code'] . '-' . $fax_no['area'] . '-' . $fax_no['num']; ?></p>
												<?php } ?>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-6">
												<label for="">Email</label>
												<p><?php echo $email; ?></p>
											</div>
											<div class="col-xs-6">
												<label for="">Website</label>
												<p><?php echo $website; ?></p>
											</div>
										</div>

										<div class="row">
											<div class="col-xs-12">
												<label for="">Company Address</label>
												<?php echo $company_address; ?>
											</div>
										</div>

										<div class="row">
											<div class="acf-field col-xs-12">
												<div class="acf-label">
													<label></label>
												</div>
												<div class="acf-input">
													<legend>Delegates</legend>
												</div>
											</div>
										</div>

										<?php
											$numItems = count($delegates);
											$x = 0;
										?>
										<?php foreach($delegates as $delegate) { ?>
											<div class="row">
												<div class="col-xs-4">
													<label for="">Title</label>
													<p><?php echo $delegate['title']; ?></p>
												</div>
												<div class="col-xs-4">
													<label for="">First Name</label>
													<p><?php echo $delegate['first_name']; ?></p>
												</div>
												<div class="col-xs-4">
													<label for="">Last Name</label>
													<p><?php echo $delegate['last_name']; ?></p>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-4">
													<label for="">Nickname</label>
													<p><?php echo $delegate['nickname']; ?></p>
												</div>
												<div class="col-xs-4">
													<label for="">Name to Appear in Badge</label>
													<p><?php echo $delegate['name_to_appear_in_badge']; ?></p>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-4">
													<label for="">Designation</label>
													<p><?php echo $delegate['designation']; ?></p>
												</div>
												<div class="col-xs-4">
													<label for="">Email</label>
													<p><?php echo $delegate['email']; ?></p>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-4">
													<label for="">Mobile No</label>
													<?php
														$delegate['mobile_no'] = array_filter($delegate['mobile_no']);
														if (!empty($delegate['mobile_no'])) {
													?>
														<p><?php echo $delegate['mobile_no']['code'] . '-' . $delegate['mobile_no']['area'] . '-' . $delegate['mobile_no']['num']; ?></p>
													<?php } ?>
												</div>
												<div class="col-xs-4">
													<label for="">Telephone No 1</label>
													<?php
														$delegate['telephone_no_1'] = array_filter($delegate['telephone_no_1']);
														if (!empty($delegate['telephone_no_1'])) {
													?>
														<p><?php echo $delegate['telephone_no_1']['code'] . '-' . $delegate['telephone_no_1']['area'] . '-' . $delegate['telephone_no_1']['num']; ?></p>
													<?php } ?>
												</div>
												<div class="col-xs-4">
													<label for="">Telephone No 2</label>
													<?php
														$delegate['telephone_no_2'] = array_filter($delegate['telephone_no_2']);
														if (!empty($delegate['telephone_no_2'])) {
													?>
														<p><?php echo $delegate['telephone_no_2']['code'] . '-' . $delegate['telephone_no_2']['area'] . '-' . $delegate['telephone_no_2']['num']; ?></p>
													<?php } ?>
												</div>
											</div>

											<div class="row">
												<div class="col-xs-4">
													<label for="">Hotel</label>
													<?php echo $delegate['hotel']; ?>
												</div>
												<div class="col-xs-4">
													<label for="">Check in date</label>
													<p><?php echo $delegate['check_in_date']; ?></p>
												</div>
												<div class="col-xs-4">
													<label for="">Check out date</label>
													<p><?php echo $delegate['check_out_date']; ?></p>
												</div>
											</div>

											<?php if(++$i === $numItems) { ?>

											<?php } else { ?>
												<div class="row">
													<div class="col-xs-12">
														<hr/>
													</div>
												</div>
											<?php } ?>
										<?php } ?>

										<!--
										<div class="row">
											<div class="acf-field col-xs-12">
												<div class="acf-label">
													<label></label>
												</div>
												<div class="acf-input">
													<legend>Payment Details</legend>
												</div>
											</div>
										</div>
										-->
									</div>
								</div>
							</div>
							<div class="col-sm-2">
								<a href="<?php bloginfo( 'url' ); ?>/seminars" class="logoutbutton">Seminars</a>
								<a href="<?php echo wp_logout_url(home_url()); ?>" class="logoutbutton">Logout</a>
							</div>
					<?php
							}
						}
						} else {
							//if not logged in
						?>
						<div class="col-sm-12">
							<div class="row">
								<div class="micecon-registration-form">
									<div class="row">
										<div class="acf-field col-xs-12">
											<div class="acf-label">
												<label></label>
											</div>
											<div class="acf-input">
												<legend>Login</legend>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-push-3 col-xs-6">
									<?php wp_login_form(); ?>
									<a href="<?php bloginfo( 'url' ); ?>/forgot-password">Forgot Password</a>
								</div>
							</div>
						</div>

						<?php
						}
					?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>

		</div>
	</div>
	<!-- END CONTENT -->

<?php get_footer(); ?>
<?php get_header(); ?>

	<!-- CONTENT -->
	<div class="col-lg-12">
		<div class="content">
			<?php if(have_posts()): ?>
				<?php while(have_posts()): the_post(); ?>
					<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
	<!-- END CONTENT -->

<?php get_footer(); ?>
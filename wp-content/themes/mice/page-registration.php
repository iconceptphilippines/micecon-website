<?php
/*
 * Template Name: Registration Form
 *
 */
add_filter('acf/load_field', 'my_acf_load_field');
add_filter('acf/validate_value', 'custom_field_validation', 10, 4);

acf_form_head();
get_header(); ?>

	<!-- CONTENT -->
	<div id="page-content">
		<div class="container">

			<div class="content">
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
						<h2 class="bordered-title colored show-grid"><?php the_title(); ?></h2>
						<div class="clearfix show-grid"></div>
						<?php
							$date = 1;
							if( $date = 1 ){ ?>

							<div class="col-sm-10">
								<div class="row">
									<h3>Online Registration is now closed.</h3>
									<p>Please email the MICECON 2015 Secretariat-Registration Committee:</p>
									<p>micecon2015@tpb.gov.ph</p>
								</div>
							</div>

						<?
							} else {
						?>

						<div class="col-sm-10">
							<div class="row">
								<?php the_content(); ?>
							</div>
						</div>

						<div class="col-sm-10">
							<div class="row">
								<?php acf_form(
									array(
										'post_id'				=> 'new_post',
										'new_post'				=> array(
																	'post_type'		=> 'users',
																	'post_status'	=> 'publish'
																),
										'field_groups'			=> array(182, 190, 282, 340),
										'form_attributes'		=> array(
																	'class'	=> 'micecon-registration-form'
																),
										'return' 				=> get_permalink().'/registration-successful',
										'html_before_fields'	=> '<div class="row">',
										'html_after_fields'		=> '</div>',
										'submit_value'			=> __('Submit', 'acf'),
										'updated_message'		=> __('Your registration was successfully submitted.', 'acf'),
									)
								); ?>
							</div>
						</div>

						<?php } ?>

					<?php endwhile; ?>
				<?php endif; ?>
			</div>

		</div>
	</div>
	<!-- END CONTENT -->

<?php get_footer(); ?>
<?php get_header(); ?>

	<!-- CONTENT -->
	<div class="content">
		<div class="show-grid2 clearfix"></div>
		<div id="welcome">
			<div class="container">
					<?php
						wp_reset_postdata();
						$welcome_arg = array(
							'post_type'			=> 'page',
							'pagename'			=> 'welcome-to-m-i-c-e-con-2015',
							'posts_per_page'	=> 1
						);
						$welcome = new WP_Query($welcome_arg);
						if($welcome->have_posts()):
							while($welcome->have_posts()): $welcome->the_post();
								the_title( '<h2 class="bordered-title">', '</h2>' );
								the_content();
							endwhile;
						endif;
						wp_reset_postdata();
					?>
			</div>
		</div>
		<!-- END WELCOME -->
		<div class="show-grid2 clearfix"></div>

		<?php
			wp_reset_postdata();
			$event_arg = array(
				'post_type'			=> 'page',
				'pagename'			=> 'date-venue',
				'posts_per_page'	=> 1
			);
			$event = new WP_Query($event_arg);
		?>
		<?php if($event->have_posts()): ?>
			<?php while($event->have_posts()): $event->the_post(); ?>
			<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
				<div id="event" style="background-image: url(<?php echo $image_url[0]; ?>);">
					<div class="show-grid2 clearfix"></div>
					<div class="container">
						<?php the_title( '<h2 class="bordered-title">', '</h2>' ); ?>
						<div class="row clearfix">
							<div class="col-md-4">
								<?php
									if(have_rows('event_details')):
										echo '<ul class="event-details">';
										while(have_rows('event_details')): the_row();
											echo '<li><span class="detail-icon"><img src="'.get_sub_field('detail_icon').'" alt=""></span><span class="detail-text">'.get_sub_field('details').'</span></li>';
										endwhile;
										echo '</ul>';
									endif;
								?>
							</div>
							<div class="col-md-8">
								<div class="event-location">
									<?php the_field('event_location'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; wp_reset_postdata(); ?>
		<!-- END EVENT -->


		<div id="news">
			<div class="container">
				<?php
					wp_reset_postdata();
					$latest_news_array = array();
					$latest_new_arg = array(
						'post_type'			=> 'news',
						'posts_per_page'	=> 4,
						'orderby'			=> 'date',
						'order'				=> 'desc'
					);
					$latest_news = new WP_Query($latest_new_arg);
					if($latest_news->have_posts()):
						while($latest_news->have_posts()): $latest_news->the_post();
							$latest_news_array[] = $post->ID;
						endwhile;
					endif;
					wp_reset_postdata();
				?>
				<div class="clearfix"></div>
				<div class="news-grid-cont clearfix">
					<div class="col-md-3">
						<div class="row">
							<?php if(!empty($latest_news_array[0])): ?>
								<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $latest_news_array[0] ), 'medium' ); ?>
								<div class="news-grid news-image" style="background-image: url(<?php echo $image_url[0]; ?>);">
									<?php if(!empty($image_url[0])): ?>
										<img src="<?php echo $image_url[0]; ?>" class="img-responsive" alt="">
									<?php endif; ?>
								</div>
								<div class="news-grid news-details tl-arrow">
									<div class="news-item-wrap">
										<span class="news-item-date text-primary"><?php echo get_field( 'news_date', $latest_news_array[0] ); ?></span>
										<h3 class="news-item-title"><?php echo get_the_title( $latest_news_array[0] ); ?></h3>
										<?php echo short_text(100, get_post_field( 'post_content', $latest_news_array[0] )); ?>
										<a href="<?php if(get_field('news_links', $latest_news_array[0]) != ''){ echo get_field( 'news_link', $latest_news_array[0] ); } else { echo get_permalink( $latest_news_array[0] ); } ?>" class="news-item-button">read more</a>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<?php if(!empty($latest_news_array[1])): ?>
								<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $latest_news_array[1] ), 'medium' ); ?>
								<div class="col-md-6 col-md-push-6">
									<div class="row">
										<div class="news-grid news-image" style="background-image: url(<?php echo $image_url[0]; ?>);">
											<?php if(!empty($image_url[0])): ?>
												<img src="<?php echo $image_url[0]; ?>" class="img-responsive" alt="">
											<?php endif; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-md-pull-6">
									<div class="row">
										<div class="news-grid news-details rt-arrow">
											<div class="news-item-wrap">
												<span class="news-item-date text-primary"><?php echo get_field( 'news_date', $latest_news_array[1] ); ?></span>
												<h3 class="news-item-title"><?php echo get_the_title( $latest_news_array[1] ); ?></h3>
												<?php echo short_text(100, get_post_field( 'post_content', $latest_news_array[1] )); ?>
												<a href="<?php if(get_field('news_links', $latest_news_array[1]) != ''){ echo get_field( 'news_link', $latest_news_array[0] ); } else { echo get_permalink( $latest_news_array[0] ); } ?>" class="news-item-button">read more</a>
											</div>
										</div>
									</div>
								</div>
							<?php endif; ?>
						</div>

						<div class="row">
							<?php if(!empty($latest_news_array[2])): ?>
								<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $latest_news_array[2] ), 'medium' ); ?>
								<div class="col-md-6">
									<div class="row">
										<div class="news-grid news-image" style="background-image: url(<?php echo $image_url[0]; ?>);">
											<?php if(!empty($image_url[0])): ?>
												<img src="<?php echo $image_url[0]; ?>" class="img-responsive" alt="">
											<?php endif; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="news-grid news-details lt-arrow">
											<div class="news-item-wrap">
												<span class="news-item-date text-primary"><?php echo get_field( 'news_date', $latest_news_array[2] ); ?></span>
												<h3 class="news-item-title"><?php echo get_the_title( $latest_news_array[2] ); ?></h3>
												<?php echo short_text(100, get_post_field( 'post_content', $latest_news_array[2] )); ?>
												<a href="<?php if(get_field('news_links', $latest_news_array[2]) != ''){ echo get_field( 'news_link', $latest_news_array[0] ); } else { echo get_permalink( $latest_news_array[0] ); } ?>" class="news-item-button">read more</a>
											</div>
										</div>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-3">
						<div class="row">
							<?php if(!empty($latest_news_array[3])): ?>
								<div class="news-grid news-details bl-arrow">
									<div class="news-item-wrap">
										<span class="news-item-date text-primary"><?php echo get_field( 'news_date', $latest_news_array[0] ); ?></span>
										<h3 class="news-item-title"><?php echo get_the_title( $latest_news_array[3] ); ?></h3>
										<?php echo short_text(100, get_post_field( 'post_content', $latest_news_array[3] )); ?>
										<a href="<?php if(get_field('news_links', $latest_news_array[3]) != ''){ echo get_field( 'news_link', $latest_news_array[0] ); } else { echo get_permalink( $latest_news_array[0] ); } ?>" class="news-item-button">read more</a>
									</div>
								</div>
								<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $latest_news_array[3] ), 'medium' ); ?>
								<div class="news-grid news-image" style="background-image: url(<?php echo $image_url[0]; ?>);">
									<?php if(!empty($image_url[0])): ?>
										<img src="<?php echo $image_url[0]; ?>" class="img-responsive" alt="">
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>

				<p class="text-center hidden"><a href="<?php bloginfo( 'url' ); ?>/news/" class="news-view-all-btn">VIEW ALL</a></p>
			</div>
		</div>
		<!-- END NEWS -->

		<?php
			wp_reset_postdata();
			$app_arg = array(
				'post_type'			=> 'page',
				'pagename'			=> 'app-section',
				'posts_per_page'	=> 1
			);
			$app = new WP_Query($app_arg);
			if($app->have_posts()):
				while($app->have_posts()): $app->the_post(); ?>
				<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
					<div id="mobile-app" style="background-image: url(<?php echo $image_url[0]; ?>);">
						<div class="container">
							<div class="col-md-10 col-md-push-1">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				<?php endwhile;
			endif;
			wp_reset_postdata();
		?>
		<!-- END APP SECTION -->

	</div>
	<!-- END CONTENT -->

<?php get_footer(); ?>
<?php
/**
 * Custom functions that use for the template/theme you are using.
 **/

/**
 * Shorten text base on character count.
 **/
function short_text($count,$text){
	$limit = $count;
	$get_text = strip_tags($text);
	$get_text = strip_shortcodes( $get_text );
	$get_text = str_split($get_text);
	$text_count = count($get_text);

	if($text_count >= $limit){
		$new_text = array_slice($get_text, 0, $limit);
		$new_text = implode( "",$new_text ).'...';
		$result = '<p>'.$new_text.'</p>';
	} else {
		$result = $text;
	}

	return $result;
}

add_filter('post_row_actions','remove_users_action_row', 10, 2);
function remove_users_action_row($actions, $post){
	//check for your post type
	if ($post->post_type == "users"){
		unset($actions['edit']);
		unset($actions['inline hide-if-no-js']);
		unset($actions['trash']);
	}
	return $actions;
}

function custom_main_query($query){

	if( $query->is_main_query() && is_post_type_archive( 'seminars' ) && !is_admin() ){
		$query->set('post_parent','0');
		// $query->set('meta_key','date');
		$query->set('orderby', 'menu_order');
		$query->set('order','ASC');
	}
	return $query;
}
add_action( 'pre_get_posts', 'custom_main_query' );

function custom_style_admin(){ ?>

	<style type="text/css">
		.edit-user-icon, .download-user-icon, .delete-user-icon { color: #454545;font-size: 25px;outline: none; }
		.edit-user-icon:hover,
		.edit-user-icon:active,
		.edit-user-icon:focus,
		.download-user-icon:hover,
		.download-user-icon:active,
		.download-user-icon:focus,
		.delete-user-icon:hover,
		.delete-user-icon:active,
		.delete-user-icon:focus { color: #7c7c7c;outline: none; }
		.actions.column-actions a { margin-right: 8px;outline: none; }
		.actions.column-actions a:hover,
		.actions.column-actions a:active,
		.actions.column-actions a:focus { outline: none;box-shadow: none; }
		.backend-visible { display: block; }
	</style>

	<link rel="icon" type="image/png" href="<?php bloginfo( 'template_url' ); ?>/micecon-favicon.png" />


	<?php
		if( isset( $_GET['post'] ) ) {
			if( get_post_type( $_GET['post'] ) == 'users' && is_admin() ){ ?>
				<style type="text/css">
					#post-body-content { margin-bottom: 0; }
				</style>
			<?php }
		}
	?>

<?php
}
add_action( 'admin_head', 'custom_style_admin' );

/*
 *   ACF CUSTOM HOOKS
 *
 */
function my_acf_load_field( $field ) {
	// var_dump($field);
	$field['wrapper']['width'] = false;
	if($field['type'] != 'true_false'){
		$field['class'] = 'form-control';
	}
	return $field;
}
// acf/load_field - filter for every field

function custom_field_validation($valid, $value, $field, $input){
	if(!$valid){
		return $valid;
	}

	if($field['type'] == 'select' && $field['required'] == 1){
		if($value == '0'){
			$valid = $field['label'].' value is required';
		}
	}

	return $valid;
}
// add_filter('acf/validate_value', 'custom_field_validation', 10, 4);

function confirm_password($valid, $value, $field, $input){

	if(isset($_POST['acf']['field_5590fa561a4c7'])){
		$password = $_POST['acf']['field_5590fa561a4c7'];
	} else {
		$password = '';
	}

	if($value != $password){
		$valid = 'Password mismatch';
	}
	return $valid;
}
add_filter('acf/validate_value/key=field_5590fa691a4c8', 'confirm_password', 10, 4);

function username_check($valid, $value, $field, $input){

	if(username_exists($value)){
		$valid = 'Username already exists';
	}

	return $valid;
}
add_filter('acf/validate_value/key=field_5590fa481a4c6', 'username_check', 10, 4);

function user_email_check($valid, $value, $field, $input){
	if( !is_admin() ):
		if(email_exists($value)){
			$valid = 'Email already exists';
		}
	endif;

	return $valid;
}
add_filter('acf/validate_value/key=field_559225571c839', 'user_email_check', 10, 4);


function testdatepciker() {
	wp_enqueue_script( 'test-datepicker-js', get_template_directory_uri() . '/js/acf-scripts.js' );
}
add_action( 'acf/input/admin_enqueue_scripts', 'testdatepciker' );

function disable_username( $field ){

	if(is_admin() && isset($_GET['post'])){
		$field['disabled'] = 1;
	}

	return $field;
}
add_filter( 'acf/load_field/key=field_5590fa481a4c6', 'disable_username' );

function remove_choices_payment_method( $field ){

	if( !is_admin() ) {
		unset($field['choices']['Paid']);
		unset($field['choices']['Unpaid']);
	}


	return $field;
}
add_filter( 'acf/load_field/key=field_559a20205f7b8', 'remove_choices_payment_method' );

function mice_mart_delegate_field( $field ){
	if(isset($_GET['post']) && is_admin()){
		$post = get_post( $_GET['post'] );
		if($post->post_type == 'users'){
			$counter = 1;
			if(have_rows('delegate')):
				while(have_rows('delegate')): the_row();
					$name = get_sub_field( 'nickname', $_GET['post'] ).' '.get_sub_field( 'last_name', $_GET['post'] );
					$field['choices'][$name] = $name;
					if($name == get_post_meta( $_GET['post'], 'mice_mart_delegate', true )){
						$field['default_value'][get_post_meta( $_GET['post'], 'mice_mart_delegate', true )] = get_post_meta( $_GET['post'], 'mice_mart_delegate', true );
					}
					$counter++;
				endwhile;
			endif;
			/*echo '<pre>';
			var_dump( $field );
			echo '</pre>';*/
			// $field['choices'][] = get_field('mice_mart_delegate', $_GET['post']);
		}
	}

	return $field;
}
add_filter( 'acf/load_field/key=field_559b3aba305e7', 'mice_mart_delegate_field' );

function mice_mart_co_delegate_field( $field ){
	if(isset($_GET['post']) && is_admin()){
		$post = get_post( $_GET['post'] );
		if($post->post_type == 'users'){
			$counter = 1;
			if(have_rows('delegate')):
				while(have_rows('delegate')): the_row();
					$name = get_sub_field( 'nickname', $_GET['post'] ).' '.get_sub_field( 'last_name', $_GET['post'] );
					$field['choices'][$name] = $name;
					if($name == get_post_meta( $_GET['post'], 'mice_mart_co-delegate', true )){
						$field['default_value'][get_post_meta( $_GET['post'], 'mice_mart_co-delegate', true )] = get_post_meta( $_GET['post'], 'mice_mart_co-delegate', true );
					}
					$counter++;
				endwhile;
			endif;
			/*echo '<pre>';
			var_dump( $field );
			echo '</pre>';*/
			// $field['choices'][] = get_field('mice_mart_delegate', $_GET['post']);
		}
	}

	return $field;
}
add_filter( 'acf/load_field/key=field_559b3b0b305e8', 'mice_mart_co_delegate_field' );

function name_to_appear_in_badge( $field ){
	$field['readonly'] = 1;

	return $field;
}
add_filter( 'acf/load_field/key=field_5593624085bfc', 'name_to_appear_in_badge' );

function form_data() {
  if ( !is_admin() ) {
   return;
  }

  $id = filter_input(INPUT_GET, 'post', FILTER_VALIDATE_INT);

  if ( !$id ) {
    return;
  }
?>
 <input type="hidden" value="<?php echo esc_attr( $id ); ?>" name="_acfobjectid">
<?php
}
add_filter( 'acf/input/form_data', 'form_data' );

function empty_password_backend( $field ){

	if(is_admin() && isset($_GET['post'])){
		// $field['disabled'] = 1;
		$field['value'] = '';
	}

	return $field;
}
add_filter( 'acf/load_field/key=field_5590fa561a4c7', 'empty_password_backend' );

function empty_check_password_backend( $field ){

	if(is_admin() && isset($_GET['post'])){
		// $field['disabled'] = 1;
		$field['value'] = '';
	}

	return $field;
}
add_filter( 'acf/load_field/key=field_5590fa691a4c8', 'empty_check_password_backend' );

function user_edit_password($valid, $value, $field, $input){
  $participant_id = filter_input(INPUT_POST, '_acfobjectid', FILTER_VALIDATE_INT); // FROM ADMIN post.php

  // For Admin only
  if ( !is_null( $participant_id ) ) {

   // If password is empty make it valid
   if ( $field['required'] && empty( $value ) ) {

    return true;
   }
  }
  if(!$valid){
  	return $valid;
  }
  return $valid;
}
add_filter('acf/validate_value/key=field_5590fa561a4c7', 'user_edit_password', 10, 4);

function user_edit_check_password($valid, $value, $field, $input){
  $participant_id = filter_input(INPUT_POST, '_acfobjectid', FILTER_VALIDATE_INT); // FROM ADMIN post.php

  // For Admin only
  if ( !is_null( $participant_id ) ) {
    if(isset($_POST['acf']['field_5590fa561a4c7'])){
    	if($_POST['acf']['field_5590fa561a4c7'] != '' || !empty($_POST['acf']['field_5590fa561a4c7'])){
    		if($value != $_POST['acf']['field_5590fa561a4c7']){
				$valid = 'Password mismatch';
			} else {
				return $valid;
			}
			return $valid;
		} else {
		    // If password is empty make it valid
		    if ( $field['required'] && empty( $value ) ) {

		    return true;
		   }

		}
    }
  }
  if(!$valid){
  	return $valid;
  }
  return $valid;
}
add_filter('acf/validate_value/key=field_5590fa691a4c8', 'user_edit_check_password', 10, 4);

function acf_password_update_value( $value, $post_id, $field ){
	if(empty($value) || $value == ''){
		$value = get_post_meta( $post_id, 'password', true );
	}

	return $value;
}
add_filter('acf/update_value/key=field_5590fa561a4c7', 'acf_password_update_value', 10, 3);

function my_acf_save_post( $post_id ) {

	$p = get_post( $post_id );
	if($p->post_type == 'users'){
		if($p->post_date == $p->post_modified){
			$username = get_field('username', $post_id);
			$password = get_field('password', $post_id);
			$email = get_field('email', $post_id);

			$status = get_post_meta( $post_id, 'user_status', true );
			update_post_meta( $post_id, '_prev_status', $status );

			$user_id = wp_create_user( $username, $password, $email );

			update_user_meta( $user_id, 'show_admin_bar_front', 'false' );
			$user_first_name = get_field('company_name', $post_id);
			update_user_meta( $user_id, 'first_name', $user_first_name );

			update_field('field_559b8831cf19c', $post_id);
			if($user_id){
				wp_update_post(array('ID' => $post_id, 'post_author' => $user_id, 'post_title' => get_field('company_name', $post_id)));
			}
		} else {
			// $username = get_field('username', $post_id);
			$password = get_field('password', $post_id);
			if(empty($password) || $password == ''){
				$change_password = get_post_meta( $post_id, 'password', true );
			} else {
				$change_password = get_field('password', $post_id);
			}
			$email = get_field('email', $post_id);
			$author_id = $p->post_author;
			// update_field('password', $change_password, $post_id);
			// update_post_meta( $post_id, 'password', $change_password, $prev_value );
			wp_update_post( array( 'ID' => $post_id, 'post_title' => get_field('company_name', $post_id) ) );

			$userdata = array(
				'ID'			=> $author_id,
				'user_pass'		=> $change_password,
				'user_email'	=> $email
			);
			if ( $author_id != 1 ){
				wp_update_user( $userdata );
			$user_first_name = get_field('company_name', $post_id);
			update_user_meta( $author_id, 'first_name', $user_first_name );
			}

			if( get_post_meta( $p->ID, '_prev_status', true ) != get_post_meta( $p->ID, 'user_status', true ) && get_post_meta( $p->ID, 'user_status', true ) == 'approved_invited' ) {
				$to = get_field( 'email', $post_id );
				$from = get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>';
				$subject = get_field( 'subject', 'option' );
				$message = get_field( 'user_approved_message', 'option' );
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset: utf8' . "\r\n";
				$headers .= "From:" . $from;
				wp_mail( $to, $subject, $message, $headers );
			}
		}

	}
}
add_action('acf/save_post', 'my_acf_save_post', 20);

function before_save_post( $post_id ){

	$p = get_post( $post_id );
	if($p->post_type == 'users'){
		if($p->post_date == $p->post_modified){
			// update_post_meta( $post_id, $meta_key, $meta_value, $prev_value );
			// update_post_meta( $post_id, '_prev_status', 'pending' );
		} else {
			$status = get_post_meta( $post_id, 'user_status', true );
			update_post_meta( $post_id, '_prev_status', $status );
		}
	}
}
add_action( 'acf/save_post', 'before_save_post', 1 );

/**
 * ACF Sub-page option
 */
if( function_exists('acf_add_options_sub_page') )
{
    acf_add_options_sub_page(array(
        'title' => 'Email Settings',
        'parent' => 'edit.php?post_type=users',
        'capability' => 'manage_options'
    ));
}


/**
 * Custom Authentication for Userlogin
 * @param  string $user     username
 * @param  string $password password
 * @return string return error if the user is rejected or pending in status
 */
function custom_login_authenticate ($user, $password) {
    //do any extra validation stuff here

	if( !in_array('administrator', $user->roles) ){

		$post_arg = array(
			'post_type'		=> 'users',
			'author'		=> $user->ID
		);
		$user_info = new WP_Query($post_arg);
		if($user_info->have_posts()):
			while ($user_info->have_posts()): $user_info->the_post();
				if( get_field('user_status') == 'approved_invited' && in_array('subscriber', $user->roles) ){
					// return;
					// echo 'test';
					// echo 'Login failed. Account not yet accepted or account is rejected';
					//return wp_redirect( get_bloginfo( 'url' ).'/seminars' );
				} else if( get_field('user_status') == 'pending' && in_array('subscriber', $user->roles) ){
					return new WP_Error( 'pending', __( "User is waiting for admin approval.", "micecon" ) );
				} else {
					return new WP_Error( 'rejected', __( "Invalid username or password.", "micecon" ) );
				}
			endwhile;
		endif;
	}

    return $user;
}
add_filter('wp_authenticate_user', 'custom_login_authenticate',10,2);


/**
 * Delete M.I.C.E Con User post and registered user.
 */
/*add_action( 'admin_init', 'codex_init' );
function codex_init() {
    if ( current_user_can( 'delete_posts' ) ){
    	add_action( 'delete_post', 'delete_post_user', 10 );
    }
}

function delete_post_user( $post_id ){

	if( get_post_type( $post_id ) != 'users' ){
		return true;
	}

	$post = get_post( $post_id );
	if( get_post_type( $post_id ) == 'users' ){
		if(did_action( 'delete_post' ) <= 2){
			wp_delete_user( $post->post_author );
		}
	}
}*/
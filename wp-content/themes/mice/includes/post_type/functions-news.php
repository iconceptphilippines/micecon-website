<?php
function news_post_type() {

	$labels = array(
		'name'                => __( 'News', 'micecon' ),
		'singular_name'       => __( 'News', 'micecon' ),
		'add_new'             => _x( 'Add New News', 'micecon', 'micecon' ),
		'add_new_item'        => __( 'Add New News', 'micecon' ),
		'edit_item'           => __( 'Edit News', 'micecon' ),
		'new_item'            => __( 'New News', 'micecon' ),
		'view_item'           => __( 'View News', 'micecon' ),
		'search_items'        => __( 'Search News', 'micecon' ),
		'not_found'           => __( 'No News found', 'micecon' ),
		'not_found_in_trash'  => __( 'No News found in Trash', 'micecon' ),
		'parent_item_colon'   => __( 'Parent News:', 'micecon' ),
		'menu_name'           => __( 'News', 'micecon' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => true,
		'description'         => 'description',
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'rewrite'             => array( 'slug' => _x( 'news', 'URL slug' ) ),
		'capability_type'     => 'post',
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions' )
	);

	register_post_type( 'news', $args );
}

add_action( 'init', 'news_post_type' );
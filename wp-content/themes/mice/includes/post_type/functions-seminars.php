<?php
function seminars_type() {

	$labels = array(
		'name'                => __( 'Seminars', 'micecon' ),
		'singular_name'       => __( 'Seminar', 'micecon' ),
		'add_new'             => _x( 'Add New Seminar', 'micecon', 'micecon' ),
		'add_new_item'        => __( 'Add New Seminar', 'micecon' ),
		'edit_item'           => __( 'Edit Seminar', 'micecon' ),
		'new_item'            => __( 'New Seminar', 'micecon' ),
		'view_item'           => __( 'View Seminar', 'micecon' ),
		'search_items'        => __( 'Search Seminar', 'micecon' ),
		'not_found'           => __( 'No Seminars found', 'micecon' ),
		'not_found_in_trash'  => __( 'No Seminars found in Trash', 'micecon' ),
		'parent_item_colon'   => __( 'Parent Seminar:', 'micecon' ),
		'menu_name'           => __( 'Seminar', 'micecon' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => true,
		'description'         => 'description',
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'rewrite'             => array( 'slug' => _x( 'seminars', 'URL slug' ) ),
		'capability_type'     => 'page',
		'supports'            => array( 'title', 'page-attributes', 'author', 'revisions' )
	);

	register_post_type( 'seminars', $args );
}

add_action( 'init', 'seminars_type' );

add_filter( 'manage_seminars_posts_columns', 'seminars_columns' ) ;
function seminars_columns( $columns ) {

	$columns = array(
		'cb'				=> '',
		'title'				=> __( 'Title' ),
		'breakout-date'		=> __( 'Date' ),
		'breakout-time'		=> __( 'Time' ),
		'max-participants'	=> __( 'Max Participants' )
	);

	return $columns;
}


add_action( 'manage_seminars_posts_custom_column' , 'seminars_custom_columns', 10, 2 );
function seminars_custom_columns( $columns, $post_id ) {
	$p = get_post( $post_id );
	switch ( $columns ) {
		case 'breakout-date':
			the_field( 'date', $post_id );
			break;

		case 'breakout-time':
			the_field( 'time', $post_id );
			break;

		case 'max-participants':
			the_field( 'max_participants', $post_id );
			break;
	}
}
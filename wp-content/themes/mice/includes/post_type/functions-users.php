<?php
function users_post_type() {

	$labels = array(
		'name'                => __( 'Users', 'micecon' ),
		'singular_name'       => __( 'User', 'micecon' ),
		'add_new'             => _x( 'Add New User', 'micecon', 'micecon' ),
		'add_new_item'        => __( 'Add New User', 'micecon' ),
		'edit_item'           => __( 'Edit User', 'micecon' ),
		'new_item'            => __( 'New User', 'micecon' ),
		'view_item'           => __( 'View User', 'micecon' ),
		'search_items'        => __( 'Search Users', 'micecon' ),
		'not_found'           => __( 'No User found', 'micecon' ),
		'not_found_in_trash'  => __( 'No User found in Trash', 'micecon' ),
		'parent_item_colon'   => __( 'Parent User:', 'micecon' ),
		'menu_name'           => __( 'M.I.C.E Con', 'micecon' ),
		'all_items'			  => __( 'Users', 'micecon' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'publicly_queryable'  => false,
		'exclude_from_search' => false,
		'has_archive'         => false,
		'query_var'           => true,
		'rewrite'             => array( 'slug' => _x( 'users', 'URL slug' ) ),
		'capability_type'     => 'post',
		'supports'            => array( 'author', 'revisions' )
	);

	register_post_type( 'users', $args );


}
add_action( 'init', 'users_post_type' );

add_filter( 'manage_users_posts_columns', 'users_columns' ) ;
function users_columns( $columns ) {

	$columns = array(
		'registerd'			=> __( 'Registered' ),
		'company'			=> __( 'Company Name' ),
		'classification'	=> __( 'Classification' ),
		'payment'			=> __( 'Payment Method' ),
		'username'			=> __( 'Username' ),
		'password'			=> __( 'Password' ),
		'status'			=> __( 'Status' ),
		'actions'			=> __( 'Actions' )
	);

	return $columns;
}

add_action( 'manage_edit-users_sortable_columns' , 'users_sortable_columns' );
function users_sortable_columns( $columns ){
	$columns = array(
		'company' => 'company',
		'registerd' => 'registered',
		'classification' => 'classification'
	);
	return $columns;
}


add_action( 'manage_users_posts_custom_column' , 'users_custom_columns', 10, 2 );
function users_custom_columns( $columns, $post_id ) {
	$p = get_post( $post_id );
	switch ( $columns ) {
		case 'registerd':
			the_time( 'M d, Y' );
			echo '<br>';
			the_time( 'h:i a' );
			break;

		case 'company':
			the_field( 'company_name', $post_id );
			break;

		case 'classification':
			the_field( 'classificationcategory', $post_id );
			break;

		case 'payment':
			the_field( 'payment_method', $post_id );
			break;

		case 'username':
			the_field( 'username', $post_id );
			break;

		case 'password':
			the_field( 'password', $post_id );
			break;

		case 'status':
			if(get_field( 'user_status', $post_id )):
				the_field( 'user_status', $post_id );
			else:
				echo 'pending';
			endif;
			break;

		case 'actions':
			if($p->post_status == 'publish' || $p->post_status == 'draft'){
				echo '<a href="'.get_edit_post_link( $post_id ).'"><span class="dashicons dashicons-welcome-write-blog edit-user-icon"></span></a>';
				echo '<a href=""><span class="dashicons dashicons-download download-user-icon"></span></a>';
				echo '<a href="'.get_delete_post_link( $post_id, '', true ).'" onclick="return confirm(\'Are you sure you want to delete user?\');"><span class="dashicons dashicons-trash delete-user-icon"></span></a>';
				// echo '<a href="'.get_delete_post_link( $post_id, '', true ).'"><span class="dashicons dashicons-trash delete-user-icon"></span></a>';
			} else {
				$restore_url = wp_nonce_url( admin_url( 'post.php?post=' . $post_id . '&action=untrash' ), 'untrash-post_' . $post_id );
				// $delete_url = wp_nonce_url( admin_url( 'post.php?post='.$post_id.'&action=delete' ), 'delete-post_'.$post_id, '_wpnonce');
				$delete_url = get_delete_post_link( $post_id, '', true );
				echo '<a href="'.$restore_url.'"><span class="dashicons dashicons-backup restor-user"></span></span></a>';
				echo '<a href="'.$delete_url.'"><span class="dashicons dashicons-trash delete-user-icon"></span></a>';
			}
			break;
	}
}

add_filter( 'pre_get_posts','posts_column_orderby');
function posts_column_orderby( $query ) {
    if( ! is_admin() )
        return;

    if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {

        //alter the query args
        switch( $orderby ) {
        	case 'company':
				$query->set('meta_key','company_name');
				$query->set('orderby','meta_value');
        		break;
        	case 'registered':
        		$query->set('orderby','date');
        		break;
        	case 'classification':
				$query->set('meta_key','classificationcategory');
				$query->set('orderby','meta_value');
        		break;
        }
    }
}



add_action('admin_menu' , 'users_report_menu'); 
function users_report_menu() {
    add_submenu_page('edit.php?post_type=users', 'Report Generations', 'Reports', 'edit_posts', 'users_report_generate', 'users_report_generate');
}

function users_report_generate() {

	$templates = array(
		'badges' => 'Badge Names',
		'lists' => 'Lists',
		'payments' => 'Payments',
		'summary' => 'Summary Report',
		'basicreport' => 'Basic Report',
		'seminars' => 'Breakout Sessions'
	);

	$current_link = filter_input( INPUT_GET, 'template' );
	$default_link = 'badges';
	$current_link_label = '';
	$formurl = "/wp-content/themes/mice/includes/generate-reports/excel-generate/excel-generate.php";
	// If current link is not set set default;
	if ( !$current_link ) {
		
		$current_link = $default_link;
	}

	if ( isset( $templates[$current_link] ) ) {
		
		$current_link_label = $templates[$current_link];
	}

	?>
		<div class="wrap">
			<h2>Reports</h2>

			<h2 class="nav-tab-wrapper">
				<a href="#" class="nav-tab nav-tab-active">Excel</a>
			</h2>

		<ul class="subsubsub">

			<?php foreach( $templates as $k => $v ): ?>

				<li><a <?php echo $k == $current_link ? 'class="current"' : '' ?> href="<?php echo esc_url( add_query_arg( array( 'template' => $k ) ) ) ?>"><?php echo $v; ?></a> | </li>

			<?php endforeach; ?>

		</ul>
		<div style="clear: both;"></div>

		<?php 
		/**
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* Badges
		*/
		if ( $current_link == 'badges' ): ?>

			<h3><?php echo $current_link_label; ?></h3>
			<p class="description">
				This exports the badge names of the approved delegates into an excel(.xlxs) file
			</p>
			
			<form target="_new" action="<?php echo site_url($formurl); ?>" method="GET" class="form-table">
				<?php wp_nonce_field( 'export-excel', 'export-excel-nonce' ); ?>
				<input type="hidden" name="report-type" value="Badges">
				<input type="hidden" name="user-status" value="approved_invited">
				<p>
					<input type="submit" value="Download Excel File" class="button button-primary">
				</p>

			</form>

		<?php endif ?>

		<?php  
		/**
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* Lists
		*/
		if ( $current_link == 'lists' ):
		?>

			<h3><?php echo $current_link_label; ?></h3>
			<p class="description">
				This exports the approved users into lists format.
			</p>

			<form target="_new" action="<?php echo site_url($formurl); ?>" method="GET" class="form-table">
				<?php wp_nonce_field( 'export-excel', 'export-excel-nonce' ); ?>
				<input type="hidden" name="report-type" value="List">
				
				<p>User Status
				<select name="user-status" id="">
					<option value="all">All</option>
					<option value="approved_invited">Approved</option>
					<option value="pending">Pending</option>
					<option value="rejected">Rejected</option>
				</select>
				</p>
				<p>
					<input type="submit" value="Download Excel File" class="button button-primary">
				</p>

			</form>

		<?php endif; ?>
		<?php  
		/**
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* Payments
		*/
		if ( $current_link == 'payments' ):
		?>

			<h3><?php echo $current_link_label; ?></h3>
			<p class="description">
				This exports the list of users with the payment information.
			</p>

			<form target="_new" action="<?php echo site_url($formurl); ?>" method="GET" class="form-table">
				<?php wp_nonce_field( 'export-excel', 'export-excel-nonce' ); ?>
				<input type="hidden" name="report-type" value="Payments">
				
				<p>User Status
				<select name="user-status" id="">
					<option value="all">All</option>
					<option value="approved_invited">Approved</option>
					<option value="pending">Pending</option>
					<option value="rejected">Rejected</option>
				</select>
				</p>
				<p>Payment Status
				<select name="payment-status" id="">
					<option value="all">All</option>
					<option value="Personal Direct Payment">Personal Direct Payment</option>
					<option value="Bank Deposit">Bank Deposit</option>
					<option value="Others">Others</option>
					<option value="Paid">Paid</option>
					<option value="Unpaid">Unpaid</option>
				</select>
				</p>
				<p>
					<input type="submit" value="Download Excel File" class="button button-primary">
				</p>

			</form>

		<?php endif; ?>
		<?php  
		/**
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* Summary Reports
		*/
		if ( $current_link == 'summary' ):
		?>

			<h3><?php echo $current_link_label; ?></h3>
			<p class="description">
				This exports the list of users with the payment information.
			</p>

			<form target="_new" action="<?php echo site_url($formurl); ?>" method="GET" class="form-table">
				<?php wp_nonce_field( 'export-excel', 'export-excel-nonce' ); ?>
				<input type="hidden" name="report-type" value="Summary">
				<input type="hidden" name="user-status" value="all">

				<p>
					<input type="submit" value="Download Excel File" class="button button-primary">
				</p>

			</form>

		<?php endif; ?>
		<?php  
		/**
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* Basic Reports
		*/
		if ( $current_link == 'basicreport' ):
		?>

			<h3><?php echo $current_link_label; ?></h3>
			<p class="description">
				This exports the list of users with the payment information.
			</p>

			<form target="_new" action="<?php echo site_url($formurl); ?>" method="GET" class="form-table">
				<?php wp_nonce_field( 'export-excel', 'export-excel-nonce' ); ?>
				<input type="hidden" name="report-type" value="BasicReport">
				<input type="hidden" name="user-status" value="all">

				<p>
					<input type="submit" value="Download Excel File" class="button button-primary">
				</p>

			</form>

		<?php endif; ?>
		<?php  
		/**
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* ************************************************************
		* Seminars Reports
		*/
		if ( $current_link == 'seminars' ):
		?>

			<h3><?php echo $current_link_label; ?></h3>
			<p class="description">
				This exports the list of delegates with their chosen breakout sessions
			</p>

			<form target="_new" action="<?php echo site_url($formurl); ?>" method="GET" class="form-table">
				<?php wp_nonce_field( 'export-excel', 'export-excel-nonce' ); ?>
				<input type="hidden" name="report-type" value="Seminars">
				<input type="hidden" name="user-status" value="approved_invited">
				<select name="seminars-report-type" id="">
					<option value="delegatelist">Delegate List with Selected Breakout Sessions</option>
					<option value="seminarlist">Breakout Sessions Lists</option>
				</select>
				<p>
					<input type="submit" value="Download Excel File" class="button button-primary">
				</p>

			</form>

		<?php endif; ?>
		</div><!--end wrap-->

		
	<?php
}
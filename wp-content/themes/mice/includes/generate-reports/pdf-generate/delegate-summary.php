<?php
require('../../exporter/mpdf/mpdf.php');
require( '../../../../../../wp-load.php' );
global $wpdb;



$user_id = $_GET['user'];
$delegate = $_GET['delegate'];

$arg = array(
	'post_type'			=> 'users',
	'posts_per_page'	=> 1,
	'author'			=> $user_id
);
$query = new WP_Query($arg);

if( $query->have_posts() ):
	while( $query->have_posts() ): $query->the_post();

		$delegates = get_field( 'delegate', $post->ID );

		$result = '<style type="text/css">';
		$result .= '* { font-family: ; }';
		$result .=	'.document-heading { text-align: center;text-transform: uppercase;margin-bottom: 50px; }';
		$result .=	'.company-name { text-transform: uppercase; }';
		$result .=	'.delegate-name { text-transform: capitalize; }';
		$result .=	'.breakout-session { border-bottom: 1px solid #484848;padding: 15px; }';
		$result .=	'.breakout-session:last-of-type { border-bottom: none; }';
		$result .= '</style>';

		$result .= '<div class="company-info">';
		$result .= '<h2 class="document-heading">Breakout Session Summary</h2>';
		$result .= '<h3 class="delegate-name">'.$delegates[$delegate]['first_name'].' '.$delegates[$delegate]['last_name'].'</h3>';
		$result .= '<p><strong>Designation:</strong> '.$delegates[$delegate]['designation'].'</p>';
		$result .= '<p><strong>Company:</strong> '.get_field( 'company_name', $post->ID ).'</p>';
		$result .= '<p><strong>Address:</strong> '.get_field( 'company_address', $post->ID ).'</p><br>';

		$delegate_selections = $delegates[$delegate]['breakout_session'];

		if( !empty($delegate_selections) ):
			$result .= '<h4>Breakout Session</h4>';
			foreach ($delegate_selections as $delegate_selection) {
				$selection_parent_id = wp_get_post_parent_id( $delegate_selection );

				$result .= '<div class="breakout-session">';
					$result .= '<p><strong>Topic:</strong> <em>'.get_the_title( $delegate_selection ).'</em></p>';
					$result .= '<p><strong>Date:</strong> '.get_field( 'date', $selection_parent_id ).'</p>';
					$result .= '<p><strong>Time:</strong> '.get_field( 'time', $selection_parent_id ).'</p>';
				$result .= '</div>';
			}
		endif;

		$result .= '</div>';

		$filename = get_field( 'company_name', $post->ID ).' Delegate Summary.pdf';
	endwhile;
endif;

$mpdf = new mpdf();
$mpdf->WriteHTML( $result );
$mpdf->Output( $filename, 'D' );
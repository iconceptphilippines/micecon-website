<?php 
require('../../exporter/phpExcel/PHPExcel.php');
require( '../../../../../../wp-load.php' );
global $wpdb;

if ( 
    ! isset( $_GET['export-excel-nonce'] ) 
    || ! wp_verify_nonce( $_GET['export-excel-nonce'], 'export-excel' ) 
) {

   //print 'Sorry, your nonce did not verify.';
   exit;

} else {

	$report_type = $_GET['report-type'];
	$user_status = $_GET['user-status'];

	if($user_status == "all") {
		$user_value = array("approved_invited", "pending", "rejected");
	} else {
		$user_value = array($user_status);
	}

	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set document properties
	$objPHPExcel->getProperties()->setCreator("M.I.C.E. Reports")
								 ->setLastModifiedBy("M.I.C.E. Reports")
								 ->setTitle($report_type. "Report");

	function cellColor($cells,$color){
		global $objPHPExcel;

		$objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				'rgb' => $color
			)
		));
	}

	if($report_type == "List") {


		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Philippine M.I.C.E. Conference (MICECON 2015)')
					->setCellValue('A2', $report_type.' Names');

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:N2');

		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A4', 'Company')
					->setCellValue('B4', 'Delegates Name')
					->setCellValue('C4', 'Surname')
					->setCellValue('D4', 'Delegate Mobile No.')
					->setCellValue('E4', 'Delegate Tel No. 1')
					->setCellValue('F4', 'Delegate Tel No. 2')
					->setCellValue('G4', 'Hotel Name')
					->setCellValue('H4', 'Address')
					->setCellValue('I4', 'Company Email')
					->setCellValue('J4', 'Status')
					->setCellValue('K4', 'Telephone No')
					->setCellValue('L4', 'Fax No')
					->setCellValue('M4', 'Username')
					->setCellValue('N4', 'Password');

		cellColor('A4:N4', 'FBDC2C');

		$classics = $wpdb->get_results(
			"SELECT DISTINCT(meta_value) 
			FROM $wpdb->postmeta
			WHERE meta_key = 'classificationcategory'
			ORDER BY meta_value"
		);

		if($classics){
			$z = 5;
			foreach ( $classics as $classic ) {
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, $classic->meta_value);
				
				cellColor('A'.$z.':M'.$z, 'CCCCCC');
				
				$z++;

				$compares = $classic->meta_value;

				$args = array(
					'post_type'			=> 'users',
					'posts_per_page'	=> -1,
					'meta_key'			=> 'classificationcategory',
					'orderby'			=> 'meta_value',
					'order'				=> 'ASC',
					'meta_query'		=> array(
						'relation'		=> 'AND',
						array(
							'key'		=> 'classificationcategory',
							'value'		=> array("$compares"),
							'compare'	=> 'IN',
						),
						array(
							'key'		=> 'user_status',
							'value'		=> $user_value,
							'compare'	=> 'IN',
						),
					),
				);

				$query = new WP_Query( $args );
				if( $query->have_posts()) {
					//content query
					while ( $query->have_posts() ) {
						$query->the_post();
						$postid = get_the_ID();
						$company_name 			= get_field('company_name', $postid);
						$classificationcategory = get_field('classificationcategory', $postid);
						$country 				= get_field('country', $postid);
						$province 				= get_field('province', $postid);
						$city 					= get_field('city', $postid); 
						$telephone_no			= array_filter(get_field('telephone_no', $postid));
						$fax_no 				= array_filter(get_field('fax_no', $postid));
						$email 					= get_field('email', $postid);
						$website 				= get_field('website', $postid);
						$company_address 		= strip_tags(get_field('company_address', $postid));
						$delegates 				= get_field('delegate', $postid);
						$username 				= get_field('username', $postid);
						$password 				= get_field('password', $postid);
						$user_status			= get_field('user_status', $postid);

						if($user_status == "") {
							$user_status = "pending";
						}

						$tel_no = "";
						if (!empty($telephone_no)) {
							foreach($telephone_no as $t) {
								$tel_no .= $t;
							}
						}

						$f_no = "";
						if (!empty($fax_no)) {
							foreach($fax_no as $f) {
								$f_no .= $f;
							}
						}




						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, $company_name);
						
						$delegatename = "";
						$lastname = "";
						$mobile_no = "";
						$tel_no_1 = "";
						$tel_no_2 = "";
						$hotel_names = "";
						$numItems = count($delegates);
						$i = 0;
						foreach($delegates as $delegate) {
							if(++$i === $numItems) {
								$delegatename .= $delegate['nickname'];
								$lastname .= $delegate['last_name'];
								$mobile_no .= $delegate['mobile_no']['code']." ".$delegate['mobile_no']['area']." ".$delegate['mobile_no']['num'];
								$tel_no_1 .= $delegate['telephone_no_1']['code']." ".$delegate['telephone_no_1']['area']." ".$delegate['telephone_no_1']['num'];
								$tel_no_2 .= $delegate['telephone_no_2']['code']." ".$delegate['telephone_no_2']['area']." ".$delegate['telephone_no_2']['num'];
								$hotel_names .= trim(preg_replace('/\s+/', ' ', strip_tags($delegate['hotel'])));
							} else {
								$delegatename .= $delegate['nickname'] . "\n";
								$lastname .= $delegate['last_name'] . "\n";
								$mobile_no .= $delegate['mobile_no']['code']." ".$delegate['mobile_no']['area']." ".$delegate['mobile_no']['num'] . "\n";
								$tel_no_1 .= $delegate['telephone_no_1']['code']." ".$delegate['telephone_no_1']['area']." ".$delegate['telephone_no_1']['num'] . "\n";
								$tel_no_2 .= $delegate['telephone_no_2']['code']." ".$delegate['telephone_no_2']['area']." ".$delegate['telephone_no_2']['num'] . "\n";
								$hotel_names .= trim(preg_replace('/\s+/', ' ', strip_tags($delegate['hotel']))) . "\n";
							}
						}


						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$z, $delegatename);
						$objPHPExcel->setActiveSheetIndex(0)->getStyle('B'.$z)->getAlignment()->setWrapText(true);

						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$z, $lastname);
						$objPHPExcel->setActiveSheetIndex(0)->getStyle('C'.$z)->getAlignment()->setWrapText(true);

						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$z, $mobile_no);
						$objPHPExcel->setActiveSheetIndex(0)->getStyle('D'.$z)->getAlignment()->setWrapText(true);

						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$z, $tel_no_1);
						$objPHPExcel->setActiveSheetIndex(0)->getStyle('E'.$z)->getAlignment()->setWrapText(true);

						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$z, $tel_no_2);
						$objPHPExcel->setActiveSheetIndex(0)->getStyle('F'.$z)->getAlignment()->setWrapText(true);

						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$z, $hotel_names);
						$objPHPExcel->setActiveSheetIndex(0)->getStyle('G'.$z)->getAlignment()->setWrapText(true);

						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$z, $company_address);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$z, $email);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$z, $user_status);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$z, $tel_no);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$z, $f_no);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$z, $username);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$z, $password);

						$z++;
					}
				}

			}
		}

		//set autowidth
		foreach(range('A','M') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				->setAutoSize(true);
		}

		//set bold to headers
		$styleArray = array(
			'font' => array(
				'bold' => true
			)
		);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1")->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A2")->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A4:N4")->getFont()->setBold(true);

	//end checking if List
	} elseif($report_type == "Badges") {

		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Philippine M.I.C.E. Conference (MICECON 2015)')
					->setCellValue('A2', $report_type.' Names')
					->setCellValue('A3', 'Conference Delegates - Name to Appear on Badge (Approved)');

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:G2');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:G3');

		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A5', '#')
					->setCellValue('B5', 'Badge Name')
					->setCellValue('C5', 'Last Name')
					//->setCellValue('D5', 'Designation')
					->setCellValue('D5', 'Company Name')
					->setCellValue('E5', 'E-mail')
					->setCellValue('F5', 'Company Email');

		cellColor('A5:F5', 'FBDC2C');

		//Query
		$args = array(
			'post_type'			=> 'users',
			'meta_key'			=> 'user_status',
			'posts_per_page'	=> -1,
			'order'				=> 'ASC',
			'meta_query'		=> array(
				array(
					'key'		=> 'user_status',
					'value'		=> $user_value,
					'compare'	=> 'IN',
				),
			),
		);
		$query = new WP_Query( $args );
		if( $query->have_posts()) {
			$z = 6;
			$x = 0;
			//content query
			while ( $query->have_posts() ) {
				$query->the_post();
				$postid = get_the_ID();
				$company_name 			= get_field('company_name', $postid);
				$email 					= get_field('email', $postid);
				$delegates 				= get_field('delegate', $postid);

				foreach($delegates as $delegate) {
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, $x);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$z, $delegate['nickname']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$z, $delegate['last_name']);
					//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$z, $delegate['designation']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$z, $company_name);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$z, $delegate['email']);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$z, $email);
					$z++;
					$x++;
				}
			}
		}


		//set autowidth
		foreach(range('A','F') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				->setAutoSize(true);
		}

		//set bold to headers
		$styleArray = array(
			'font' => array(
				'bold' => true
			)
		);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1")->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A2")->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A3")->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A5:F5")->getFont()->setBold(true);

	} elseif($report_type == "Payments") {

		$payment_status = $_GET['payment-status'];

		if($payment_status == "all") {
			$payment_value = array("Personal Direct Payment", "Bank Deposit", "Others", "Paid", "Unpaid");
		} else {
			$payment_value = array($payment_status);
		}

		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Philippine M.I.C.E. Conference (MICECON 2015)')
					->setCellValue('A2', $report_type.' List')
					->setCellValue('A3', 'Conference Delegates - Payment Information (Approved)');

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C1');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:C2');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:C3');

		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A5', 'Company Name')
					->setCellValue('B5', 'Payment Date')
					->setCellValue('C5', 'Payment Method')
					->setCellValue('D5', 'Amount')
					->setCellValue('E5', 'OR #')
					->setCellValue('F5', 'Remarks');

		cellColor('A5:F5', 'FBDC2C');

		$args = array(
			'post_type'			=> 'users',
			'posts_per_page'	=> -1,
			'meta_key'			=> 'payment_method',
			'orderby'			=> 'meta_value',
			'order'				=> 'ASC',
			'meta_query'		=> array(
				'relation'		=> 'AND',
				array(
					'key'		=> 'payment_method',
					'value'		=> $payment_value,
					'compare'	=> 'IN',
				),
				array(
					'key'		=> 'user_status',
					'value'		=> $user_value,
					'compare'	=> 'IN',
				),
			),
		);

		$query = new WP_Query( $args );
		if( $query->have_posts()) {
			$z = 6;
			//content query
			while ( $query->have_posts() ) {
				$query->the_post();
				$postid = get_the_ID();
				$company_name 	= get_field('company_name', $postid);
				$payment_date 	= get_field('payment_date', $postid);
				$payment_method = get_field('payment_method', $postid);
				$amount 		= get_field('amount', $postid);
				$or_no 			= get_field('or_#', $postid);
				$remarks 		= get_field('remarks', $postid);
				

				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, $company_name);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$z, $payment_date);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$z, $payment_method);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$z, $amount);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$z, $or_no);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$z, $remarks);
				$z++;
			}
		}



		//set autowidth
		foreach(range('A','F') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				->setAutoSize(true);
		}

		//set bold to headers
		$styleArray = array(
			'font' => array(
				'bold' => true
			)
		);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1")->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A2")->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A3")->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A5:F5")->getFont()->setBold(true);

	} elseif($report_type == "Summary") {
		
		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Philippine M.I.C.E. Conference (MICECON 2015)')
					->setCellValue('A2', $report_type.' List')
					->setCellValue('A3', 'Summary of M.I.C.E. Con Participants - Paid & Unpaid (All)');

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:B1');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:B2');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:B3');

		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A5', 'Classifications')
					->setCellValue('B5', 'Total Number of Registrants');

		cellColor('A5:B5', 'FBDC2C');

		$classics = $wpdb->get_results(
			"SELECT DISTINCT(meta_value) 
			FROM $wpdb->postmeta
			WHERE meta_key = 'classificationcategory'
			ORDER BY meta_value"
		);

		if($classics){
			$z = 6;
			$total_count = 0;

			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, "Registered Companies");
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$z)->getFont()->setBold(true);
			cellColor('A'.$z.':B'.$z, 'CCCCCC');
			$z++;
			foreach ( $classics as $classic ) {

				$sql = "SELECT count(DISTINCT pm.post_id)
				FROM $wpdb->postmeta pm
				JOIN $wpdb->posts p ON (p.ID = pm.post_id)
				WHERE pm.meta_key = 'classificationcategory'
				AND pm.meta_value = '$classic->meta_value'
				AND p.post_type = 'users'
				AND p.post_status = 'publish'
				";
				$count = $wpdb->get_var($sql);

				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, $classic->meta_value);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$z, $count);
				$total_count = $total_count + $count;
				$z++;
			}
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, "Total Count");
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$z)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$z, $total_count);

			$z = $z + 2;
			$total_count = 0;

			$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$z, 'Delegates Registered')
					->setCellValue('B'.$z, 'Total');

			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$z)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B'.$z)->getFont()->setBold(true);
			cellColor('A'.$z.':B'.$z, 'FBDC2C');

			$z++;

			$delegates_count = $wpdb->get_results("SELECT meta_value
			FROM $wpdb->postmeta pm
			JOIN $wpdb->posts p ON (p.ID = pm.post_id)
			WHERE pm.meta_key = 'delegate'
			AND p.post_type = 'users'
			AND p.post_status = 'publish'");

			if($delegates_count){
				foreach($delegates_count as $x) {
					$total_count = $total_count + $x->meta_value;
				}
			}
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, "Total Delegates Registered");
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$z)->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$z, $total_count);
			cellColor('A'.$z.':B'.$z, 'CCCCCC');

			$z = $z + 2;
			$total_count = 0;

			/* fetching of total number of paid users per classification and payment methods */
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, "Payment Methods Summary");
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$z)->getFont()->setBold(true);
			cellColor('A'.$z.':B'.$z, 'FBDC2C');
			$z++;

			
			foreach ( $classics as $classic ) {

				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, $classic->meta_value);
				$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$z)->getFont()->setBold(true);
				cellColor('A'.$z.':B'.$z, 'CCCCCC');
				$z++;
				$paymethods = $wpdb->get_results(
					"SELECT DISTINCT(meta_value) 
					FROM $wpdb->postmeta
					WHERE meta_key = 'payment_method'
					ORDER BY meta_value"
				);

				if($paymethods){

					foreach ( $paymethods as $paymethod ) {

						if(($paymethod->meta_value == "paid") || ($paymethod->meta_value == "0")) {

						} else {

							$sql_new = "SELECT count(DISTINCT wposts.ID)
							FROM $wpdb->posts wposts
							LEFT JOIN $wpdb->postmeta wpm1 ON (wposts.ID = wpm1.post_id
							                       AND wpm1.meta_key = 'classificationcategory')
							LEFT JOIN $wpdb->postmeta wpm2 ON (wposts.ID = wpm2.post_id
							                       AND wpm2.meta_key = 'payment_method')
							WHERE
							((wpm1.meta_value = '$classic->meta_value') AND (wpm2.meta_value = '$paymethod->meta_value'))
							AND wposts.post_status = 'publish'
							AND wposts.post_type = 'users'";

							$pay_count = $wpdb->get_var($sql_new);

							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, $paymethod->meta_value);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$z, $pay_count);
							
							$z++;
						}
					}
				}
			}

		}

		//set autowidth
		foreach(range('A','B') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				->setAutoSize(true);
		}

		//set bold to headers
		$styleArray = array(
			'font' => array(
				'bold' => true
			)
		);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1")->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A2")->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A3")->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle("A5:B5")->getFont()->setBold(true);

	} elseif($report_type == "BasicReport") {

		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Classification')
					->setCellValue('B1', 'Company')
					->setCellValue('C1', 'Delegates Name')
					->setCellValue('D1', 'Surname')
					->setCellValue('E1', 'Designation')
					->setCellValue('F1', 'Delegate Mobile No.')
					->setCellValue('G1', 'Delegate Tel No. 1')
					->setCellValue('H1', 'Delegate Tel No. 2')
					->setCellValue('I1', 'Delegate Email')
					->setCellValue('J1', 'Hotel Name')
					->setCellValue('K1', 'Address')
					->setCellValue('L1', 'Province')
					->setCellValue('M1', 'Company Email')
					->setCellValue('N1', 'Status')
					->setCellValue('O1', 'Telephone No')
					->setCellValue('P1', 'Fax No')
					->setCellValue('Q1', 'Username')
					->setCellValue('R1', 'Password')
					->setCellValue('S1', 'Payment Method')
					->setCellValue('T1', 'Amount')
					->setCellValue('U1', 'Remarks')
					->setCellValue('V1', 'Registration Date');

		$args = array(
			'post_type'			=> 'users',
			'posts_per_page'	=> -1,
			'meta_key'			=> 'user_status',
			'orderby'			=> 'meta_value',
			'order'				=> 'ASC',
			'meta_query'		=> array(
				'relation'		=> 'AND',
				array(
					'key'		=> 'user_status',
					'value'		=> $user_value,
					'compare'	=> 'IN',
				),
			),
		);

		$query = new WP_Query( $args );
		if( $query->have_posts()) {
			//content query
			$z = 2;
			while ( $query->have_posts() ) {
				$query->the_post();
				$postid = get_the_ID();
				$company_name 			= get_field('company_name', $postid);
				$classificationcategory = get_field('classificationcategory', $postid);
				$country 				= get_field('country', $postid);
				$province 				= get_field('province', $postid);
				$city 					= get_field('city', $postid); 
				$telephone_no			= array_filter(get_field('telephone_no', $postid));
				$fax_no 				= array_filter(get_field('fax_no', $postid));
				$email 					= get_field('email', $postid);
				$website 				= get_field('website', $postid);
				$company_address 		= strip_tags(get_field('company_address', $postid));
				$delegates 				= get_field('delegate', $postid);
				$username 				= get_field('username', $postid);
				$password 				= get_field('password', $postid);
				$user_status			= get_field('user_status', $postid);
				$payment_date 			= get_field('payment_date', $postid);
				$payment_method 		= get_field('payment_method', $postid);
				$amount 				= get_field('amount', $postid);
				$or_no 					= get_field('or_#', $postid);
				$remarks 				= strip_tags(get_field('remarks', $postid));
				$registered 			= get_user_by('login', $username);
				$registered_date		= $registered->user_registered;

				if($user_status == "") {
					$user_status = "pending";
				}

				$tel_no = "";
				if (!empty($telephone_no)) {
					foreach($telephone_no as $t) {
						$tel_no .= $t;
					}
				}

				$f_no = "";
				if (!empty($fax_no)) {
					foreach($fax_no as $f) {
						$f_no .= $f;
					}
				}
				
				foreach($delegates as $delegate) {
					$delegatename = $delegate['first_name'];
					$lastname = $delegate['last_name'];
					$mobile_no = $delegate['mobile_no']['code']." ".$delegate['mobile_no']['area']." ".$delegate['mobile_no']['num'];
					$tel_no_1 = $delegate['telephone_no_1']['code']." ".$delegate['telephone_no_1']['area']." ".$delegate['telephone_no_1']['num'];
					$tel_no_2 = $delegate['telephone_no_2']['code']." ".$delegate['telephone_no_2']['area']." ".$delegate['telephone_no_2']['num'];
					$designation = $delegate['designation'];
					$delegate_emil = $delegate['email'];
					$hotel_names = trim(preg_replace('/\s+/', ' ', strip_tags($delegate['hotel'])));

					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, $classificationcategory);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$z, $company_name);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$z, $delegatename);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$z, $lastname);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$z, $designation);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$z, $mobile_no);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$z, $tel_no_1);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$z, $tel_no_2);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$z, $delegate_emil);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$z, $hotel_names);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$z, $company_address);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$z, $province);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$z, $email);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$z, $user_status);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$z, $tel_no);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$z, $f_no);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$z, $username);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$z, $password);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$z, $payment_method);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$z, $amount);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$z, $remarks);
					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$z, $registered_date);
					$z++;
				}
			}
		}

		//set autowidth
		foreach(range('A','V') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				->setAutoSize(true);
		}

	} elseif($report_type == "Seminars") {
		// Add some data
		if($_GET['seminars-report-type'] == "delegatelist") {
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Philippine M.I.C.E. Conference (MICECON 2015)')
						->setCellValue('A2', $report_type.' Names')
						->setCellValue('A3', 'Conference Delegates - Breakout Sessions');

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:G2');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:G3');

			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A5', '#')
						->setCellValue('B5', 'First Name')
						->setCellValue('C5', 'Last Name')
						//->setCellValue('D5', 'Designation')
						->setCellValue('D5', 'Company Name')
						->setCellValue('E5', 'E-mail')
						->setCellValue('F5', 'Company Email')
						->setCellValue('G5', 'Breakout Session');

			cellColor('A5:G5', 'FBDC2C');

			//Query
			$args = array(
				'post_type'			=> 'users',
				'meta_key'			=> 'user_status',
				'posts_per_page'	=> -1,
				'order'				=> 'ASC',
				'meta_query'		=> array(
					array(
						'key'		=> 'user_status',
						'value'		=> $user_value,
						'compare'	=> 'IN',
					),
				),
			);
			$query = new WP_Query( $args );
			if( $query->have_posts()) {
				$z = 6;
				$x = 0;
				//content query
				while ( $query->have_posts() ) {
					$query->the_post();
					$postid = get_the_ID();
					$company_name 			= get_field('company_name', $postid);
					$email 					= get_field('email', $postid);
					$delegates 				= get_field('delegate', $postid);

					foreach($delegates as $delegate) {
						$breakout_contents = "";

						$delegate_selections = $delegate['breakout_session'];
						foreach ($delegate_selections as $delegate_selection) {
							$selection_parent_id = wp_get_post_parent_id( $delegate_selection );
							
							$breakout_contents .= "Topic: " . get_the_title( $delegate_selection ) . "\n";
							$breakout_contents .= "Date: " . get_field( 'date', $selection_parent_id ) . "\n";
							$breakout_contents .= "Time: " . get_field( 'time', $selection_parent_id ) . "\n";
							$breakout_contents .= "\n";

						}

						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, $x);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$z, $delegate['first_name']);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$z, $delegate['last_name']);
						//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$z, $delegate['designation']);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$z, $company_name);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$z, $delegate['email']);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$z, $email);
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$z, $breakout_contents);
						$objPHPExcel->setActiveSheetIndex(0)->getStyle('G'.$z)->getAlignment()->setWrapText(true);
						$z++;
						$x++;
					}
				}
			}


			//set autowidth
			foreach(range('A','G') as $columnID) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
					->setAutoSize(true);
			}

			//set bold to headers
			$styleArray = array(
				'font' => array(
					'bold' => true
				)
			);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1")->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A2")->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A3")->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A5:G5")->getFont()->setBold(true);
		} elseif($_GET['seminars-report-type'] == "seminarlist") {
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1', 'Philippine M.I.C.E. Conference (MICECON 2015)')
						->setCellValue('A2', 'Breakout Sessions');

			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:B1');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:B2');

			//Query
			$args = array(
				'post_type'			=> 'seminars',
				'posts_per_page'	=> -1,
				'post_parent'		=> 0,
			);
			$query = new WP_Query( $args );
			if( $query->have_posts()) {
				$z = 4;
				$x = 0;
				while( $query->have_posts()): $query->the_post();

					$title = get_the_title();
					$the_date = get_field( 'date' );
					$parent_id = get_the_ID();

					$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, $title . " : " . $the_date);
					$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$z.':B'.$z);
					$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$z.':B'.$z)->getFont()->setBold(true);
					cellColor('A'.$z.':B'.$z, 'FBDC2C');
					$z++;

					$courses_arg = array(
						'post_type'			=> 'seminars',
						'posts_per_page'	=> -1,
						'post_parent'		=> $parent_id,
						'orderby'			=> 'id',
						'order'				=> 'ASC'
					);
					$courses = new WP_Query($courses_arg);

					if( $courses->have_posts()) {
						$course_count = 1;
						while( $courses->have_posts()): $courses->the_post();
							$new_id = get_the_ID();
							$participants_count = 0;
							$get_users_arg = array( 'post_type' => 'users', 'posts_per_page' => -1 );
							$get_users = get_posts( $get_users_arg );
							foreach ($get_users as $get_user) {
								$user_delegates = get_field( 'delegate', $get_user->ID );
								foreach ($user_delegates as $user_delegate) {
									if( !empty($user_delegate['breakout_session']) ){
										if( in_array($post->ID, $user_delegate['breakout_session']) ){
											$participants_count++;
										}
									}
								}
							}

							$the_time = get_field('time',$parent_id);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$z, $the_time);

							$the_course = "";
							$the_course .= "Course ". $course_count . ": " . strip_tags(get_field( 'course', $new_id )) . "\n";
							$the_course .= "Title: ". strip_tags(get_the_title()) . "\n";
							$the_course .= "Speaker/s: ". strip_tags(get_field( 'speakers', $new_id )). "\n";
							if( get_field( 'moderator' ) && strip_tags(get_field( 'moderator', $new_id )) != '' ){ 
								$the_course .= "Moderator:". strip_tags(get_field( 'moderator', $new_id )). "\n";
							}
							$the_course .= "Target Participants: ". strip_tags(get_field( 'target_participants', $new_id )). "\n";
							$the_course .= "Attendees: ". $participants_count ."/". get_field( 'max_participants', $new_id );

							$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$z, $the_course);
							$objPHPExcel->setActiveSheetIndex(0)->getStyle('B'.$z)->getAlignment()->setWrapText(true);
						$course_count++;
						$z++;
						endwhile;
					}

				endwhile;
			}


			//set autowidth
			foreach(range('A','B') as $columnID) {
				$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
					->setAutoSize(true);
			}

			//set bold to headers
			$styleArray = array(
				'font' => array(
					'bold' => true
				)
			);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1")->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A2")->getFont()->setBold(true);
		}
	}
	
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$report_type.'-micecon.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;


   // process form data
}
<?php
/* Template Name: Speakers */

get_header(); ?>

	<!-- CONTENT -->
	<div id="page-content">
		<div class="container">

			<div class="content">
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
						<h2 class="bordered-title colored show-grid"><?php the_title(); ?></h2>
						<div class="clearfix show-grid"></div>

						<?php // the_content(); ?>

						<div class="row">
							<?php if(have_rows('speakers')): ?>
								<?php while(have_rows('speakers')): the_row(); ?>
									<div class="col-md-4 show-grid2 custom-display">
										<div class="col-lg-4">
											<div class="row">
												<?php if( get_sub_field( 'link' ) || get_sub_field( 'link' ) != '' ){ ?>
													<a href="<?php the_sub_field( 'link' ); ?>"><img src="<?php the_sub_field('speaker_image'); ?>" class="img-responsive center-block" alt=""></a>
												<?php } else { ?>
													<img src="<?php the_sub_field('speaker_image'); ?>" class="img-responsive center-block" alt="">
												<?php } ?>
											</div>
										</div>
										<div class="col-lg-8">
											<?php the_sub_field('speaker_details'); ?>
										</div>
										<div class="clearfix"></div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>

					<?php endwhile; ?>
				<?php endif; ?>
			</div>

		</div>
	</div>
	<!-- END CONTENT -->

<?php get_footer(); ?>
			</div> <!-- END ROW -->
		</div> <!-- END CONTAINER -->
	</div>
	<!-- END CONTENT -->

	<!-- FOOTER -->
	<div id="footer">

		<div id="sponsors-section">
			<div class="container">
				<div class="row">
					<?php
						wp_reset_postdata();
						$sponsors_arg = array(
							'post_type'			=> 'page',
							'pagename'			=> 'sponsors',
							'posts_per_page'	=> 1
						);
						$sponsors = new WP_Query($sponsors_arg);
					?>
					<?php if($sponsors->have_posts()): ?>
						<?php while($sponsors->have_posts()): $sponsors->the_post(); ?>

							<?php if(!is_home()): ?>
								<div class="col-lg-12"><hr></div>
							<?php endif; ?>

							<?php if(get_field('sponsors_text_label')): ?>
								<div class="col-xs-12"><h2 class="bordered-title"><?php the_field('sponsors_text_label'); ?></h2></div>
							<?php endif; ?>

							<?php if(have_rows('sponsors')): ?>
								<?php $counter = 1; ?>
								<?php while(have_rows('sponsors')): the_row(); ?>

									<?php if($counter == 1): ?>
										<div class="col-md-7">
											<h3 class="sponsors-title"><?php the_sub_field('sponsors_label'); ?></h3>
											<?php if(have_rows('sponsors_list')): ?>
												<ul class="sponsors-logos list-inline">
													<?php while(have_rows('sponsors_list')): the_row(); ?>
														<li style="width: <?php the_sub_field('width'); ?>"><a href="<?php the_sub_field('link'); ?>" target="_blank"><img src="<?php the_sub_field('logo') ?>" class="img-responsive" alt=""></a></li>
													<?php endwhile; ?>
												</ul>
											<?php endif; ?>
										</div>

									<?php elseif($counter == 2): ?>
										<div class="col-md-5">
											<h3 class="sponsors-title"><?php the_sub_field('sponsors_label'); ?></h3>
											<?php if(have_rows('sponsors_list')): ?>
												<ul class="sponsors-logos list-inline">
													<?php while(have_rows('sponsors_list')): the_row(); ?>
														<li style="width: <?php the_sub_field('width'); ?>"><a href="<?php the_sub_field('link'); ?>" target="_blank"><img src="<?php the_sub_field('logo') ?>" class="img-responsive" alt=""></a></li>
													<?php endwhile; ?>
												</ul>
											<?php endif; ?>
										</div>

										<div class="clearfix"></div>

									<?php elseif($counter == 3): ?>
										<div class="col-md-12">
											<h3 class="sponsors-title"><?php the_sub_field('sponsors_label'); ?></h3>
											<?php if(have_rows('sponsors_list')): ?>
												<ul class="sponsors-logos list-inline">
													<?php while(have_rows('sponsors_list')): the_row(); ?>
														<li style="width: <?php the_sub_field('width'); ?>"><a href="<?php the_sub_field('link'); ?>" target="_blank"><img src="<?php the_sub_field('logo') ?>" class="img-responsive" alt=""></a></li>
													<?php endwhile; ?>
												</ul>
											<?php endif; ?>
										</div>
										<div class="clearfix"></div>

									<?php elseif($counter == 4): ?>
										<div class="col-md-6 sponsor-majorsponsors">
											<h3 class="sponsors-title"><?php the_sub_field('sponsors_label'); ?></h3>
											<?php if(have_rows('sponsors_list')): ?>
												<ul class="sponsors-logos list-inline">
													<?php while(have_rows('sponsors_list')): the_row(); ?>
														<li style="width: <?php the_sub_field('width'); ?>"><a href="<?php the_sub_field('link'); ?>" target="_blank"><img src="<?php the_sub_field('logo') ?>" class="img-responsive" alt=""></a></li>
													<?php endwhile; ?>
												</ul>
											<?php endif; ?>
										</div>

									<?php elseif($counter == 5): ?>
										<div class="col-md-6 sponsors-sponsors">
											<h3 class="sponsors-title"><?php the_sub_field('sponsors_label'); ?></h3>
											<?php if(have_rows('sponsors_list')): ?>
												<ul class="sponsors-logos list-inline">
													<?php while(have_rows('sponsors_list')): the_row(); ?>
														<li style="width: <?php the_sub_field('width'); ?>"><a href="<?php the_sub_field('link'); ?>" target="_blank"><img src="<?php the_sub_field('logo') ?>" class="img-responsive" alt=""></a></li>
													<?php endwhile; ?>
												</ul>
											<?php endif; ?>
										</div>

									<?php elseif($counter == 6): ?>
										<div class="col-md-3">
											<h3 class="sponsors-title"><?php the_sub_field('sponsors_label'); ?></h3>
											<?php if(have_rows('sponsors_list')): ?>
												<ul class="sponsors-logos list-inline">
													<?php while(have_rows('sponsors_list')): the_row(); ?>
														<li style="width: <?php the_sub_field('width'); ?>"><a href="<?php the_sub_field('link'); ?>" target="_blank"><img src="<?php the_sub_field('logo') ?>" class="img-responsive" alt=""></a></li>
													<?php endwhile; ?>
												</ul>
											<?php endif; ?>
										</div>

									<?php elseif($counter == 7): ?>
										<div class="col-md-9">
											<h3 class="sponsors-title"><?php the_sub_field('sponsors_label'); ?></h3>
											<?php if(have_rows('sponsors_list')): ?>
												<ul class="sponsors-logos list-inline">
													<?php while(have_rows('sponsors_list')): the_row(); ?>
														<li style="width: <?php the_sub_field('width'); ?>"><a href="<?php the_sub_field('link'); ?>" target="_blank"><img src="<?php the_sub_field('logo') ?>" class="img-responsive" alt=""></a></li>
													<?php endwhile; ?>
												</ul>
											<?php endif; ?>
										</div>
										<div class="clearfix"></div>

									<?php else: ?>
										<div class="col-xs-12">
											<h3 class="sponsors-title"><?php the_sub_field('sponsors_label'); ?></h3>
											<?php if(have_rows('sponsors_list')): ?>
												<ul class="sponsors-logos list-inline">
													<?php while(have_rows('sponsors_list')): the_row(); ?>
														<li style="width: <?php the_sub_field('width'); ?>"><a href="<?php the_sub_field('link'); ?>" target="_blank"><img src="<?php the_sub_field('logo') ?>" class="img-responsive" alt=""></a></li>
													<?php endwhile; ?>
												</ul>
											<?php endif; ?>
										</div>
										<div class="clearfix"></div>

									<?php endif; ?>

									<?php $counter++; ?>
								<?php endwhile; ?>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
		<!-- END SPONSORS -->

		<div id="contact-information-section">
			<div class="container">
				<div class="row">

					<?php
						$contact_arg = array(
							'post_type'			=> 'page',
							'pagename'			=> 'contact-information',
							'posts_per_page'	=> 1
						);
						$contact = new WP_Query($contact_arg);
					?>
					<?php if($contact->have_posts()): ?>
						<?php while($contact->have_posts()): $contact->the_post(); ?>
							<div class="col-xs-12"><h2 class="bordered-title"><?php the_title(); ?></h2></div>

							<?php if(have_rows('footer_widget')): ?>
								<?php while(have_rows('footer_widget')): the_row(); ?>

									<div class="col-md-3">
										<?php if(have_rows('rows')): ?>
											<ul class="footer-widget">
												<?php while(have_rows('rows')): the_row(); ?>
													<li>
														<h3 class="footer-widget-title"><?php the_sub_field('row_title'); ?></h3>
														<?php the_sub_field('row_text'); ?>
													</li>
												<?php endwhile; ?>
											</ul>
										<?php endif; ?>
									</div>

								<?php endwhile; ?>
							<?php endif; ?>

						<?php endwhile; ?>
					<?php endif; ?>

					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- END CONTACT INFORMATION SECTION -->


		<div id="copyright">
			<div class="container">
				<div class="row">
					<div class="col-sm-6"><p class="copyright">Copyright&copy;2015. M.I.C.E CON  All rights reserved.</p></div>
					<div class="col-sm-6"><p class="text-right">Cloud Hosting by <a target="_blank" href="http://zoom.ph">Zoom</a>. Web Design by <a target="_blank" href="http://iconcept.com.ph">iConcept Web Design Philippines</a><br>SEO BY <a target="_blank" href="http://iconcept-seo.com">iConcept - SEO</a></p></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END FOOTER -->

	<!--Bootstrap Modal - James -->
	<div id="myModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Online Registration is now closed.</h4>
				</div>
				<div class="modal-body">
					<p>Please email the MICECON 2015 Secretariat-Registration Committee:<br/>micecon2015@tpb.gov.ph</p>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

<?php wp_footer(); ?>
<script>
	$(document).ready(function() {
		$('.registrationModalJames').click(function() {
			$('#myModal').modal();	
		});
	});

	/**
	* Vertically center Bootstrap 3 modals so they aren't always stuck at the top
	*/
	$(document).ready(function() {
		function reposition() {
			var modal = $(this),
			dialog = modal.find('.modal-dialog');
			modal.css('display', 'block');

			// Dividing by two centers the modal exactly, but dividing by three 
			// or four works better for larger screens.
			dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
		}
		// Reposition when a modal is shown
		$('.modal').on('show.bs.modal', reposition);
		// Reposition when the window is resized
		$(window).on('resize', function() {
			$('.modal:visible').each(reposition);
		});
	});
</script>
</body>
</html>
